<html>
    <tr>
        <td colspan="5" style="background-color: #dfdfdf"><h2>Quotation Detail</h2></td>
    </tr>
    <tr>
        <td><b>Quotation number</b></td>
        <td>{{ $data->quo }}</td>
    </tr>
    <tr>
        <td><b>Project name</b></td>
        <td>{{ $data->project }}</td>
    </tr>
    <tr>
        <td><b>Project status</b></td>
        <td>{{ $data->status }}</td>
    </tr>
    <tr>
        <td><b>Consumer name</b></td>
        <td>{{ $data->c_name }}</td>
    </tr>
    <tr>
        <td><b>Project email</b></td>
        <td>{{ $data->c_email }}</td>
    </tr>
    <tr>
        <td><b>Project phone</b></td>
        <td align="left">{{ $data->c_phone }}</td>
    </tr>
    <tr>
        <td><b>Project address</b></td>
        <td>{{ $data->c_address }}</td>
    </tr>
    <tr>
        <td><b>Project time</b></td>
        <td align="left">{{ $data->time }}</td>
    </tr>
    <tr>
        <td><b>Project price</b></td>
        <td>IDR {{ number_format($data->price) }}</td>
    </tr>
    <tr>
        <td><b>Create date</b></td>
        <td>{{ $data->created_at }}</td>
    </tr>
    <tr>
        <td><b>last update</b></td>
        <td>{{ $data->updated_at }}</td>
    </tr>
    <tr></tr>
    <tr style="background-color: #dfdfdf;">
        <td colspan="5"><h2>Quotation items</h2></td>
    </tr>
    <tr>
        <td><b>No</b></td>
        <td><b>Item</b></td>
        <td><b>Price</b></td>
    </tr>
    @foreach($item as $indexKey=>$a)
    <tr>
        <td align="left">{{ $indexKey+1 }}</td>
        <td align="left">{{ $a->item }}</td>
        <td align="left">IDR {{ number_format($a->harga) }}</td>
    </tr>
    @endforeach
</html>
