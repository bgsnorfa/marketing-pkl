<html>
    <tr style="background-color: #dfdfdf">
        <td colspan="5"><h2>Quotation Details</h2></td>
    </tr>

    <tr>
        <td><b>Project</b></td>
        <td>{{ $quo->project }}</td>
    </tr>

    <tr>
        <td>Consumer name</td>
        <td>{{ $consumer->name }}</td>
    </tr>

    <tr>
        <td>Project Price</td>
        <td>IDR {{ number_format($quo->price) }}</td>
    </tr>

    <tr>
        <td>Amount Paid</td>
        <td>IDR {{ number_format($quo->paid) }}</td>
    </tr>

    <tr>
        <td>Amount Unpaid</td>
        <td>IDR {{ number_format($quo->price - $quo->paid) }}</td>
    </tr>

    <tr>
        <td></td>
    </tr>

    <tr style="background-color: #dfdfdf">
        <td colspan="5"><h2>Invoices Detail</h2></td>
    </tr>

    <tr>
        <td><b>Invoice Number</b></td>
        <td><b>Date Added</b></td>
        <td><b>Due Date</b></td>
        <td><b>Status</b></td>
        <td><b>Amount</b></td>
    </tr>
    @foreach($inv as $a)
    <tr>
        <td>{{ $a->inv }}</td>
        <td>{{ $a->created_at }}</td>
        <td>{{ $a->duedate }}</td>
        <td>{{ $a->status }}</td>
        <td>IDR {{ number_format($a->amount) }}</td>
    </tr>
    @endforeach
</html>
