<html>
    <tr style="background-color: #dfdfdf;">
        <td><b>No</b></td>
        <td><b>Consumer Name</b></td>
        <td><b>Consumer Email</b></td>
        <td><b>Consumer Phone</b></td>
        <td><b>Consumer Address</b></td>
        <td><b>Create Date</b></td>
        <td><b>Marketing name</b></td>
    </tr>
    @foreach($data as $indexKey=>$a)
    <tr>
        <td align="left">{{ $indexKey+1 }}</td>
        <td>{{ $a->name }}</td>
        <td>{{ $a->email }}</td>
        <td align="left">{{ $a->phone }}</td>
        <td>{{ $a->address }}</td>
        <td>{{ $a->created_at }}</td>
        <td>{{ $a->marketing_name }}</td>
    </tr>
    @endforeach
</html>
