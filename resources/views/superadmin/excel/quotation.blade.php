<html>
    <tr style="background-color: #dfdfdf;">
        <td><b>No</b></td>
        <td><b>Quo</b></td>
        <td><b>Consumer</b></td>
        <td><b>Project</b></td>
        <td><b>Price</b></td>
        <td><b>Status</b></td>
        <td><b>Marketing</b></td>
    </tr>
    @foreach($data as $indexKey=>$a)
    <tr>
        <td align="left">{{ $indexKey+1 }}</td>
        @if ($a->quo === '')
        <td><i>in Draft</i></td>
        @else
        <td>{{ $a->quo }}</td>
        @endif
        <td>{{ $a->c_name }}</td>
        <td>{{ $a->project }}</td>
        <td align="left">IDR {{ number_format($a->price) }}</td>
        <td>{{ $a->status }}</td>
        <td>{{ $a->user_name }}</td>
    </tr>
    @endforeach
</html>
