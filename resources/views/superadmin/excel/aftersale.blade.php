<html>
    <tr style="background-color: #dfdfdf;">
        <td><b>Quotation Number</b></td>
        <td><b>Project Name</b></td>
        <td><b>Consumer Name</b></td>
        <td><b>Start Date</b></td>
        <td><b>End Date</b></td>
        <td><b>Status</b></td>
        <td><b>Marketing</b></td>
    </tr>

    @foreach($data as $a)
    <tr>
        <td>{{ $a->q_quo }}</td>
        <td>{{ $a->q_project }}</td>
        <td>{{ $a->c_name }}</td>
        <td>{{ $a->startdate }}</td>
        <td>{{ $a->enddate }}</td>
        <td>
            <?php
            if ($a->enddate <= \Carbon\Carbon::now()) {
                $status = 'Expired';
            } else if ($a->enddate <= \Carbon\Carbon::now()->addDays(30)) {
                $end = new \Carbon\Carbon($a->enddate);
                $now = \Carbon\Carbon::now();
                $difference = $end->diff($now)->days;

                $status = 'Will finished in ' . $difference . ' days';
            } else {
                $status = 'Still on date';
            }
            ?>
            {{ $status }}
        </td>
        <td>{{ $a->u_name }}</td>
    </tr>
    @endforeach
</html>
