<html>
    <tr style="background-color: #dfdfdf;">
        <td><b>No</b></td>
        <td><b>Quotation number</b></td>
        <td><b>Consumer name</b></td>
        <td><b>Contract name</b></td>
        <td><b>Project name</b></td>
        <td><b>Marketing</b></td>
    </tr>
    @foreach($data as $indexKey=>$a)
    <tr>
        <td align="left">{{ $indexKey+1 }}</td>
        <td>{{ $a->quo }}</td>
        <td>{{ $a->c_name }}</td>
        <td>{{ $a->name }}</td>
        <td>{{ $a->q_project }}</td>
        <td>{{ $a->u_name }}</td>
    </tr>
    @endforeach
</html>
