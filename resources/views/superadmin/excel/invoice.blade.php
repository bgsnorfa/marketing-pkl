<html>
    <tr style="background-color: #dfdfdf">
        <td colspan="3"><h2>Invoice Data</h2></td>
    </tr>

    <tr>
        <td><b>Quotation number</b></td>
        <td><b>Invoice number</b></td>
        <td><b>Item</b></td>
        <td><b>Amount</b></td>
    </tr>

    @foreach($inv as $a)
    <tr>
        <td>{{ $a->quo }}</td>
        <td>{{ $a->inv }}</td>
        <td>{{ $a->item }}</td>
        <td>IDR {{ number_format($a->amount) }}</td>
    </tr>
    @endforeach

    <tr style="background-color: #dfdfdf">
        <td colspan="3">
            <?php
                $amount = 0;
                foreach ($inv as $a) {
                    $amount = $amount + $a->amount;
                }
            ?>
            <b>Total Paid : IDR {{ number_format($amount) }}</b>
        </td>
    </tr>
</html>
