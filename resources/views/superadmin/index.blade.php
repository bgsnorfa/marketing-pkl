@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
  <a href="#">Dashboard</a>
</li>
<li class="breadcrumb-item active">My Dashboard</li>
@endsection

@section('content')

  <!-- Page Heading -->
<div class="row">


      <div>
            <form class="form-inline" method="get">
                <select class="form-control" name="user">
                    <option value="">All marketing</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-info">
            </form>
        </div>
        <br>
        <div class="col-xl-12 col-sm-12 mb-3">

          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-user"></i>
                 </div>
                 <h3>
                    Consument
                </h3>
                <table class="table">
                   <tr>
                      <td> <h3> {{$consument}}</h3></td>
                      <td> <h3> {{$consument1}}</h3></td>
                      <td> <h3> {{$consument2}}</h3></td>
                  </tr>
                  <tr>
                      <td> <p> All Consument</p></td>
                      <td> <p> Consument With Quo</p></td>
                      <td> <p> Consument With Con</p></td>
                  </tr>
              </table>
            </div>
            <a href="{{ url('superadmin/consument') }}" class="card-footer text-white clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>


    <?php $value =0;?>
    @foreach($quotation as $quo)
    @if($quo->status === 'accepted')
    <?php $value = $value + $quo->price;?>
    @endif
    @endforeach

    <div class="col-xl-12 col-sm-12 mb-3">
        <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-file"></i>
                 </div>
                 <h3>
                    Quotation
                </h3>
                <table class="table">
                   <tr>
                      <td> <h3> {{$quotation1}}</h3></td>
                      <td> <h3> {{$quotation2}}</h3></td>
                      <td> <h3> {{$quotation3}}</h3></td>
                      <td> <h3> {{$quotation4}}</h3></td>
                      <td> <h3> {{ number_format($value) }}</h3></td>

                  </tr>
                  <tr>
                      <td class="col-xs-3"> <p> All Quotations</p></td>
                      <td class="col-xs-3"> <p> Quotations On-Process</p></td>
                      <td class="col-xs-3"> <p> Quotations Accepted</p></td>
                      <td class="col-xs-3"> <p> Quotations Rejected</p></td>
                      <td class="col-xs-6"> <p class="huge">  Value(Idr)</p></td>
                  </tr>
              </table>
            </div>
            <a href="{{ url('superadmin/quotation') }}" class="card-footer text-white clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>

    <div class="col-xl-12 col-sm-12 mb-3">
        <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-book"></i>
                 </div>
                 <h3>
                    Contract
                </h3>
                <table class="table">
                   <tr>
                      <td> <h3> {{$contract}}</h3></td>
                     </tr>
                  <tr>
                      <td class="col-xs-3"> <p> All Contract</p></td>
                     </tr>
              </table>
            </div>
            <a href="{{ url('superadmin/contract') }}" class="card-footer text-white clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>

    <div class="col-xl-12 col-sm-12 mb-3">
        <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-credit-card"></i>
                 </div>
                 <h3>
                    Invoice
                </h3>
                <table class="table">
                   <tr>
                      <?php $paid = 0;?>
                      <?php $total = 0;?>
                      <?php $paidvalue = 0;?>
                      <?php $unpaid = 0;?>
                      @foreach($invoice as $inv)
                      @if($inv->price === $inv->paid)
                      <?php $paid = $paid + 1;?>
                      @else
                      <?php $unpaid = $unpaid+1;?>
                      @endif
                      @endforeach

                      @foreach($invoice as $inv)
                     <?php $paidvalue = $paidvalue + $inv->paid;?>
                      <?php $total = $total + $inv->price;?>
                      @endforeach

                      <td> <h3> {{$paid}}</h3></td>
                      <td> <h3> {{$unpaid}}</h3></td>
                      <td> <h3> {{ number_format($total) }}</h3></td>
                      <td> <h3> {{ number_format($paidvalue) }}</h3></td>

                  </tr>
                  <tr>
                      <td class="col-xs-3"> <p>Paid Quotations</p></td>
                      <td class="col-xs-3"> <p>Unpaid Quotations</p></td>
                      <td class="col-xs-3"> <p> Total Value(Idr)</p></td>
                      <td class="col-xs-3"> <p> Paid Value(Idr)</p></td>
                  </tr>
              </table>
            </div>
            <a href="{{ url('superadmin/invoice') }}" class="card-footer text-white clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>

    <div class="col-xl-12 col-sm-12 mb-3">
        <div class="card text-white bg-info o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-wrench"></i>
                 </div>
                 <h3>
                    After Sale
                </h3>
                <table class="table">
                   <tr>
                      <td> <h3> {{$aftersale}}</h3></td>
                      <td> <h3> {{$aftersale1}}</h3></td>
                      <td> <h3> {{$aftersale2}}</h3></td>
                     </tr>
                  <tr>
                      <td class="col-xs-3"> <p> All After Sale</p></td>
                      <td class="col-xs-3"> <p> Active </p></td>
                      <td class="col-xs-3"> <p> Nearly Ended </p></td>
                     </tr>
              </table>
            </div>
            <a href="{{ url('superadmin/aftersale') }}" class="card-footer text-white clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>

</div>



@endsection
