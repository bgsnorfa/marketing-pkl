@extends('layouts.superadmin')

@section('breadcrumb')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/aftersale') }}">Aftersale</a>
</li>
<li class="breadcrumb-item active">
    {{ $aftersale->quo }}
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-wrench"></i> AfterSale
    </div>

    <div class="card-body">
        <table class="table table-responsive table-hover">
            <tr>
                <td>Project Name</td>
                <td>{{ $aftersale->q_project }}</td>
            </tr>
            <tr>
                <td>Consumer Name</td>
                <td>{{ $aftersale->c_name }}</td>
            </tr>
            <tr>
                <td>Quotation Number</td>
                <td>{{ $aftersale->quo }}</td>
            </tr>
            <tr>
                <td>File</td>
                <td>
                    <a class="btn btn-info" href="{{ url('aftersales/') . '/' . $aftersale->file }}">
                        <i class="fa fa-file"></i> View
                    </a>
                </td>
            </tr>
            <tr>
                <td>Start Date</td>
                <td>{{ $aftersale->startdate }}</td>
            </tr>
            <tr>
                <td>End Date</td>
                <td>{{ $aftersale->enddate }}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>
                    <?php
                    if ($aftersale->enddate <= \Carbon\Carbon::now()) {
                        $status = 'Expired';
                    } else if ($aftersale->enddate <= \Carbon\Carbon::now()->addDays(30)) {
                        $end = new \Carbon\Carbon($aftersale->enddate);
                        $now = \Carbon\Carbon::now();
                        $difference = $end->diff($now)->days;

                        $status = 'Will finished in ' . $difference . ' day(s)';
                    } else {
                        $status = 'Still on date';
                    }
                    ?>
                    {{ $status }}
                </td>
            </tr>
        </table>
    </div>

    <div class="card-footer">
        <?php
        if (url()->previous() == url()->current()) {
            $back = url('superadmin/aftersale');
        } else {
            $back = url()->previous();
        }
        ?>
        <a class="btn btn-info" href="{{ $back }}"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
</div>
@endsection
