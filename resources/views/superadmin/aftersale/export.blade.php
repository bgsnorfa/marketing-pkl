@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/aftersale') }}">AfterSale</a>
</li>
<li class="breadcrumb-item active">
    Export
</li>
@endsection


@section('content')
<div class="card mb3">
    <div class="card-header">
        Export AfterSales Data to Excel
    </div>

    <div class="card-body">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="select">
                    Select Marketing
                </label>
                <select name="as_marketing" class="form-control" id="as_marketing" required>
                    <option value="">---Select Marketing---</option>
                    <option value="all">All Marketing</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="select">
                    Select Year
                </label>
                <select name="as_year" class="form-control" id="as_year" required>
                    <option value="">---Select year---</option>
                </select>
            </div>

            <div class="form-group">
                <label for="select">
                    Select Month
                </label>
                <select name="as_month" class="form-control" id="as_month" required>
                    <option value="">---Select month---</option>
                </select>
            </div>

            <input type="submit" name="submit" class="btn btn-info">
        </form>
    </div>
</div>
@endsection
