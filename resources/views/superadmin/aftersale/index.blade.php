@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">
    After Sale
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <fa class="fa fa-wrench"></fa> After Sale
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Quotation number</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Action</th>
            </tr>
            @foreach($aftersales as $indexKey=>$a)
            <?php
            if ($a->enddate < \Carbon\Carbon::now()->addDays(30)) {
                $bg = '#dfdfdf';
            } else {
                $bg = 'transparent';
            }
            ?>
            <tr style="background-color: {{ $bg }}">
                <td>{{ $indexKey+1 }}</td>
                <td>{{ $a->quo }}</td>
                <td>{{ $a->startdate }}</td>
                <td>{{ $a->enddate }}</td>
                <td>
                    <a class="btn btn-primary" href="{{ url()->current() . '/' .  $a->id }}"><i class="fa fa-eye"></i></a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="form-footer">
        <div style="float: left;">
            {{ $aftersales->appends($_GET)->links('vendor.pagination.bootstrap-4') }}
        </div>
        <div style="float: right;">
            <form method="get" action="{{ url()->current() }}" class="form-inline">
                <select name="user" class="form-control">
                    <option value="">Marketing name</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>

                <input class="btn btn-info" type="submit" >
            </form>
        </div>

    </div>
</div>
@endsection
