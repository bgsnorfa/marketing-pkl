@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">
    Quotation
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-file"></i> Quotation
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Quo</th>
                <th>Consumer</th>
                <th>Project</th>
                <th>Price</th>
                <th>Status</th>
                <th>Marketing</th>
                <th>Action</th>
            </tr>
            @foreach($quotations as $indexKey=>$a)
            <?php
            $color = '';
            if ($a->price == $a->paid) {
                $color = '#dfdfdf';
            } else {
                if ($a->status === 'rejected') {
                    $color = '#f46242';
                }
            }
            ?>
            <tr style="background-color: {{ $color }}">
                <td>{{ $indexKey+1 }}</td>
                <td>{{ $a->quo }}</td>
                <td>{{ $a->c_name }}</td>
                <td>{{ $a->project }}</td>
                <td>IDR {{ number_format($a->price) }}</td>
                <td>{{ $a->status }}</td>
                <td>{{ $a->user_name }}</td>
                <td>
                    <a class="btn btn-info" href="{{ url('superadmin/quotation/' . $a->id) }}"><i class="fa fa-eye"></i> View</a>
                    <a class="btn btn-warning" href="{{ url('superadmin/quotation/export/' . $a->id) }}"><i class="fa fa-file-excel-o"></i> Export</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="card-footer">
        <div style="float: left;">
            {{ $quotations->appends($_GET)->links('vendor.pagination.bootstrap-4') }}
        </div>
        <div style="float: right;">
            <form class="form-inline" method="get">
                <select class="form-control" name="user">
                    <option value="">Marketing name</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-info">
            </form>
        </div>
    </div>
</div>
@endsection
