@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/quotation') }}">Quotation</a>
</li>
<li class="breadcrumb-item active">
    Draft
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-file"></i> Quotation Draft
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <!-- <th>Quo</th> -->
                <th>Consument</th>
                <th>Project</th>
                <th>Price</th>
                <th>Status</th>
                <th>Marketing</th>
                <th>Action</th>
            </tr>
            @foreach($quotations as $indexKey=>$a)
            <tr>
                <td>{{ $indexKey+1 }}</td>
                <!-- <td>{{-- $a->quo --}}</td> -->
                <td>{{ $a->c_name }}</td>
                <td>{{ $a->project }}</td>
                <td>IDR {{ number_format($a->price) }}</td>
                <td>{{ $a->status }}</td>
                <td>{{ $a->user_name }}</td>
                <td>
                    <a class="btn btn-info" href="{{ url('superadmin/quotation/draft/' . $a->id) }}"><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="card-footer">
        <div style="float: left;">
            {{ $quotations->appends($_GET)->links('vendor.pagination.bootstrap-4') }}
        </div>
        <div style="float: right;">
            <form class="form-inline" method="get">
                <select class="form-control" name="user">
                    <option value="">Marketing name</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-info">
            </form>
        </div>
    </div>
</div>
@endsection
