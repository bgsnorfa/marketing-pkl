@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('/superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/quotation') }}">Quotation</a>
</li>
@if($quotation->quo == '')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/quotation/draft') }}">Draft</a>
</li>
@endif
<li class="breadcrumb-item active">
    {{ $quotation->project }}
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-file"></i> Quotation detail
    </div>

    <div class="card-body">
        <table class="table table-bordered table-hover">
            <tr>
                <td>Quotation number</td>
                <td>{{ $quotation->quo }}</td>
            </tr>
            <tr>
                <td>Project name</td>
                <td>{{ $quotation->project }}</td>
            </tr>
            <tr>
                <td>Project status</td>
                <td>{{ $quotation->status }}</td>
            </tr>
            <tr>
                <td>Consumer name</td>
                <td>{{ $quotation->c_name }}</td>
            </tr>
            <tr>
                <td>Consumer email</td>
                <td>{{ $quotation->c_email }}</td>
            </tr>
            <tr>
                <td>Consumer phone</td>
                <td>{{ $quotation->c_phone }}</td>
            </tr>
            <tr>
                <td>Consumer address</td>
                <td>{{ $quotation->c_address }}</td>
            </tr>
            <tr>
                <td>Project time</td>
                <td>{{ $quotation->time }}</td>
            </tr>
            <tr>
                <td>Project price</td>
                <td>IDR {{ number_format($quotation->price) }}</td>
            </tr>
            <tr>
                <td>Created at</td>
                <td>{{ $quotation->created_at }}</td>
            </tr>
            <tr>
                <td>Updated at</td>
                <td>{{ $quotation->updated_at }}</td>
            </tr>
        </table>
        <h5>Quotation Items</h5>
        <table class="table table-bordered table-hover">
            <tr>
                <th>No</th>
                <th>Item</th>
                <th>Price</th>
            </tr>
            @foreach($items as $index=>$a)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $a->item }}</td>
                <td>IDR {{ number_format($a->harga) }}</td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="card-footer">
        <?php
        if (url()->previous() == url()->current()) {
            if ($quotation->quo != '') {
                $back = url('superadmin/quotation');
            } else {
                $back = url('superadmin/quotation/draft');
            }
        } else {
            $back = url()->previous();
        }
        ?>

        <a class="btn btn-info" href="{{ $back }}"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
</div>

@endsection
