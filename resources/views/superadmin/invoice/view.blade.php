@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('/superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/invoice') }}">Invoice</a>
</li>
<li class="breadcrumb-item active">
    {{ $quotation->quo }}
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-file"></i> Quotation detail
    </div>

    <div class="card-body">
        <table class="table table-bordered table-hover">
            <tr>
                <td>Consumer name:</td>
                <td>{{ $consument->name }}</td>
            </tr>
            <tr>
                <td>Project :</td>
                <td>{{ $quotation->project}}</td>
            </tr>

            <tr>
                <td>Project price:</td>
                <td>IDR {{ number_format($quotation->price) }}</td>
            </tr>

            <tr>
                <td>Amount paid:</td>
                <td>IDR {{ number_format($quotation->paid) }}</td>
            </tr>
			<tr>
                <td>Amount Unpaid:</td>
                <td>IDR {{ number_format($quotation->price-$quotation->paid) }}</td>
            </tr>
			<tr>
                <td colspan='2'><h3>Detail Invoice</h3></td>

            </tr>
			</tr>

			<tr>
                <th>Invoice Number</th>
                <th>Date Paid</th>
                <th>Amount</th>
			</tr>
		@foreach($invoice as $invoice)
			<tr>
				<td>{{$invoice->inv}}</td>
				<td>{{$invoice->created_at}}</td>
				<td>IDR {{ number_format($invoice->amount) }}</td>
			</tr>
			@endforeach

        </table>
    </div>

    <div class="card-footer">
        <?php
        if (url()->previous() == url()->current()) {
            if ($quotation->quo != '') {
                $back = url('superadmin/quotation');
            } else {
                $back = url('superadmin/quotation/draft');
            }
        } else {
            $back = url()->previous();
        }
        ?>

        <a class="btn btn-info" href="{{ $back }}"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
</div>

@endsection
