@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/invoice') }}">Invoice</a>
</li>
<li class="breadcrumb-item active">
    Export
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">Export to Excel</div>

    <div class="card-body">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="select"></label>
                <select name="inv_year" class="form-control" id="inv_year" required>
                    <option value="">---Select Year---</option>
                    @foreach($year as $a)
                    <option value="{{ $a }}">{{ $a }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="select"></label>
                <select name="inv_month" class="form-control" id="inv_month" required>
                    <option value="">---Select Month---</option>
                    @foreach($year as $a)
                    <option value="{{ $a }}">{{ $a }}</option>
                    @endforeach
                </select>
            </div>

            <input type="submit" name="submit" class="btn btn-info">
        </form>
    </div>
</div>
@endsection
