@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">
    View User
</li>
@endsection

@section('content')

@if(isset($flash))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> New User successfully created
</div>
@endif

<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-user"></i> Add new User
    </div>

    <div class="card-body">
        <table class="table bordered table-hover">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            @foreach($users as $index=>$a)
            <tr>
                <td>{{ $index+1 }}</td>
                <td>{{ $a->name }}</td>
                <td>{{ $a->email }}</td>
                <td>
                    <a class="btn btn-info" href="{{ url('superadmin/user/view/' . $a->id . '/edit') }}">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a class="btn btn-danger" href="{{ url('superadmin/user/view/' . $a->id . '/delete') }}" onclick="return confirm('Are you sure?')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="card-footer">

    </div>
</div>
@endsection
