@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">
    View User
</li>
@endsection

@section('content')

@if(isset($flash))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <strong>Success!</strong> New User successfully created
</div>
@endif

<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-user"></i> Add new User
    </div>

    <div class="card-body">
        <form method="POST">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <input type="submit" class="btn btn-success" value="Update">
        </form>
    </div>

    <div class="card-footer">

    </div>
</div>
@endsection
