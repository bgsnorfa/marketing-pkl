@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('/superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">
    Consumer
</li>
@endsection

@section('content')
<div class="card mb-3">
    <div class="card-header">
        <i class="fa fa-user"></i> Consumer Table
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Status</th>
                <th>Marketing</th>
                <th>Action</th>
            </tr>
            @foreach($consuments as $indexKey=>$a)
            <tr>
                <td>{{ $indexKey+1 }}</td>
                <td>{{ $a->name }}</td>
                <td>{{ $a->email }}</td>
                <td>{{ $a->status }}</td>
                <td>{{ $a->user_name }}</td>
                <td>
                    <a href="{{ url()->current() . '/' . $a->id }}" class="btn btn-info"><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="card-footer">
        <div style="float: left;">
            {{ $consuments->appends($_GET)->links('vendor.pagination.bootstrap-4') }}
        </div>
        <div style="float: right;">
            <form method="get" class="form-inline">
                <select class="form-control" name="user">
                    <option value="">Marketing team</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-info">
            </form>
        </div>
    </div>
</div>
@endsection
