@extends('layouts.superadmin')

@section('breadcrumbs')
<ul class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('superadmin') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ url('superadmin/consument') }}">Consument</a>
    </li>
    <li class="breadcrumb-item active">
        Export
    </li>
</ul>
@endsection

@section('content')
<div class="card mb3">

    <div class="card-header">
        Export Consuments data to Excel
    </div>

    <div class="card-body">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="select">Select year</label>
                <select name="year" class="form-control" id="consument_year" required>
                    <option value="">---select year---</option>
                    @foreach($year as $a)
                    <option value="{{ $a }}">{{ $a }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="select">Select Month</label>
                <!-- <div id="consument_month"></div> -->
                <select name="month" class="form-control" id="consument_month" required>
                    <option value="">---select month---</option>

                </select>
            </div>

            <input type="submit" name="submit" class="btn btn-info">
        </form>
    </div>

    <div class="card-footer">

    </div>

</div>
@endsection
