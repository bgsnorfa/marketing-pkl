@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('/superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('/superadmin/consument') }}">Consumer</a>
</li>
<li class="breadcrumb-item active">
    {{ $consument->name }}
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-user"></i> Consumer Detail
    </div>

    <div class="card-body">
        <table class="table table-bordered table-hover">
            <tr>
                <td>Consumer Name</td>
                <td>{{ $consument->name }}</td>
            </tr>
            <tr>
                <td>Consumer Email</td>
                <td>{{ $consument->email }}</td>
            </tr>
            <tr>
                <td>Consumer Phone</td>
                <td>{{ $consument->phone }}</td>
            </tr>
            <tr>
                <td>Consumer Address</td>
                <td>{{ $consument->address }}</td>
            </tr>
            <tr>
                <td>Consumer Status</td>
                <td>{{ $consument->status }}</td>
            </tr>
            <tr>
                <td>Marketing Name</td>
                <td>{{ $consument->user_name }}</td>
            </tr>
        </table>
    </div>

    <div class="card-footer">
        <?php
        if (url()->previous() == url()->current()) {
            $back = url('superadmin/consument');
        } else {
            $back = url()->previous();
        }
        ?>
        <a class="btn btn-info" href="{{ $back }}">
        <!-- <a class="btn btn-info" href="{{ url('superadmin/consument') }}"> -->
            <i class="fa fa-arrow-left"></i> Back
        </a>
    </div>
</div>
@endsection
