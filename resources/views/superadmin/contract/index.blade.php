@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">
    Contract
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-book"></i> Contract
    </div>

    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Quotation number</th>
                <th>Contract name</th>
                <th>Contract file</th>
                <th>Marketing</th>
                <th>Action</th>
            </tr>
            @foreach($contracts as $indexKey=>$a)
            <tr>
                <td>{{ $indexKey+1 }}</td>
                <td>{{ $a->quo }}</td>
                <td>{{ $a->name }}</td>
                <td><a class="btn btn-primary" target="_blank" href="{{ url('contracts/' . $a->file) }}"><i class="fa fa-floppy-o"></i> File</a></td>
                <td>{{ $a->u_name }}</td>
                <td>
                    <a class="btn btn-info" href="{{ url('superadmin/contract/' . $a->id) }}"><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>

    <div class="card-footer">
        <div style="float: left;">
            {{ $contracts->appends($_GET)->links('vendor.pagination.bootstrap-4') }}
        </div>
        <div style="float: right;">
            <form method="get" class="form-inline">
                <select class="form-control" name="user">
                    <option value="">Marketing name</option>
                    @foreach($marketing as $a)
                    <option value="{{ $a->id }}">{{ $a->name }}</option>
                    @endforeach
                </select>
                <input type="submit" class="btn btn-info">
            </form>
        </div>
    </div>
</div>
@endsection
