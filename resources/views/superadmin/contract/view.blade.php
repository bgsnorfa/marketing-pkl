@extends('layouts.superadmin')

@section('breadcrumbs')
<li class="breadcrumb-item">
    <a href="{{ url('superadmin') }}">Dashboard</a>
</li>
<li class="breadcrumb-item">
    <a href="{{ url('superadmin/contract') }}">Contract</a>
</li>
<li class="breadcrumb-item active">
    {{ $contract->name }}
</li>
@endsection

@section('content')
<div class="card mb3">
    <div class="card-header">
        <i class="fa fa-book"></i> Contract Detail
    </div>

    <div class="card-body">
        <table class="table table-bordered table-hover">
            <tr>
                <td>Contract name</td>
                <td>{{ $contract->name }}</td>
            </tr>
            <tr>
                <td>Quotation number</td>
                <td>{{ $contract->quo }}</td>
            </tr>
            <tr>
                <td>Consumer name</td>
                <td>{{ $contract->c_name }}</td>
            </tr>
            <tr>
                <td>Contract Description</td>
                <td>{{ $contract->desc }}</td>
            </tr>
            <tr>
                <td>Project name</td>
                <td>{{ $contract->q_project }}</td>
            </tr>
            <tr>
                <td>Contract file</td>
                <td><a class="btn btn-primary" target="_blank" href="{{ url('contracts/' . $contract->file) }}"><i class="fa fa-floppy-o"></i> File</a></td>
            </tr>
            <tr>
                <td>Marketing name</td>
                <td>{{ $contract->u_name }}</td>
            </tr>
        </table>
    </div>

    <div class="card-footer">
        <?php
        if (url()->previous() == url()->current()) {
            $back = url('superadmin/contract');
        } else {
            $back = url()->previous();
        }
        ?>
        <a class="btn btn-info" href="{{ $back }}"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
</div>
@endsection
