@extends('layouts.finance')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm text-muted" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
        <!-- /.main-search -->
    </div>

    <div class="main-bar">
        <h3><i class="fa fa-building"></i>&nbsp;Boxed Layout</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.head -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoice <small>Browse Invoice</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/finance') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-file"></i> Invoice
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
    <div class="col-lg-12">

       <table class="table table-condensed table-hover table-responsive ajaxdata" style="display:none">
        <thead>
         <tr>

            <th>No</th>
            <th>Inv</th>
            <th>Quo</th>
            <th>Item</th>
            <th>Amount</th>
            <th>Due Date</th>
            <th>Status</th>
            <th>Action</th>

        </tr>
    </thead>


    <tbody id="success">
    </tbody>
</table>
<table class="table table-hover table-condensed table-responsive generaldata">
     <thead>
         <tr>
        <th>No</th>
        <th>Inv</th>
        <th>Quo</th>
        <th>Item</th>
        <th>Amount</th>
        <th>Due Date</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
</thead>
    <?php $count = 1; ?>
    @foreach($quotation as $data)
    <tbody>

    @if ($data->duedate < Carbon\Carbon::now() && $data->status !== 'paid')
          <tr bgcolor="#FF0000">
    @else
        <tr>
    @endif
  
        <td>{{ $count }}</td>
        <td>{{ $data->inv }}</td>
        <td>{{ $data->quo }}</td>
        <td>{{ $data->item }}</td>
        <td>{{number_format($data->amount) }}</td>
        <td>{{ $data->duedate }}</td>
        <td>{{ $data->status }}</td>
        <td>
            @if($data->status === 'paid')
            <a href="{{ url('/invoices/' . $data->file) }}" class="btn btn-primary btn-xs">Show</a>
            @else
            <a href="{{ url('/finance/invoice/' .$data->id. '/payinvoice') }}" class="btn btn-xs btn-success">Pay</a>
            @endif
        </td>
    </tr>
    </tbody>
    <?php $count++; ?>
    @endforeach
</table>
</div>
</div>

<script type="text/javascript">
    function search(){
        var search =$('#search').val();
        if(search){
            $(".generaldata").hide();
            $(".ajaxdata").show();
        }else{
            $(".generaldata").show();
            $(".ajaxdata").hide();
        }
        $.ajax({
            type: "POST",
            url: '{{URL::to("/finance/invoice/search")}}',
            data: {
                search: search,
                _token: $('#signup-token').val()
            },
            datatype:'html',

            success: function(response){
                console.log(response);
                $("#success").html(response);
            }
        });
    }
</script>
@endsection
