@extends('layouts.finance1')

@section('heading')
<!-- Page Heading -->
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Invoice</h3>
    </div>
                            <!-- /.main-bar -->
        </header>


<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoice <small>{{ $quotation->quo }}</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/finance') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/finance/invoice') }}"><i class="fa fa-file"></i> Invoice</a>
            </li>
            <li class="active">
                {{ $quotation->quo }}
            </li>
        </ol>
    </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <a href="{{ url('/finance/quotation') }}"><span class="glyphicon glyphicon-arrow-left"></span> Back to Quotation</a>
    </div>
</div>

<div class="row">
    <div clas="col-lg-12">
        <table class="table table-hover">
            <tr>
                <td>Project :</td>
                <td>{{ $quotation->project}}</td>
            </tr>

            <tr>
                <td>Project price:</td>
                <td>{{ $quotation->price }}</td>
            </tr>

            <tr>
                <td>Amount paid:</td>
                <td>{{ $quotation->paid }}</td>
            </tr>
			<tr>
                <td>Amount Unpaid:</td>
                <td>{{$quotation->price-$quotation->paid }}</td>
            </tr>
			<tr>
                <td colspan='2'><h3>Detail Invoice</h3></td>

            </tr>

			<tr>
                <th>No</th>
                <th>Invoice No</th>
                <th>Date Paid</th>
                <th>Amount</th>
			</tr>
			<?php $count = 1; ?>
		@foreach($invoice as $invoice)
			<tr>
				<td>{{$count}}</td>
				<td>{{$invoice->inv}}</td>
				<td>{{$invoice->created_at}}</td>
				<td>{{$invoice->amount}}</td>
			</tr>
			<?php $count =$count+ 1; ?>
			@endforeach
		</table>
    </div>
</div>
@endsection
