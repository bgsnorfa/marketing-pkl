<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lokavor Studio</title>

    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">

    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />

    <link href="{{ asset('marketing-template/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('marketing-template/css/sb-admin.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('marketing-template/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Bootstrap -->
    <link href="{{ asset('marketing-template1/lib/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Metis core stylesheet -->
    <link href="{{ asset('marketing-template1/css/main.css') }}" rel="stylesheet">

    <!-- metisMenu stylesheet -->
    <link href="{{ asset('marketing-template1/lib/metismenu/metisMenu.css') }}" rel="stylesheet">

    <!-- onoffcanvas stylesheet -->
    <link href="{{ asset('marketing-template1/lib/onoffcanvas/onoffcanvas.css') }}" rel="stylesheet">

    <!-- animate.css stylesheet -->
    <link href="{{ asset('marketing-template1/lib/animate.css/animate.css') }}" rel="stylesheet">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.5.1/themes/prism.min.css">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->



<link href="{{ asset('marketing-template1/less/theme.less') }}" rel="stylesheet/less">
<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script>

</head>

<body>
    <div class="bg-dark dk" id="wrap">
        <div id="top">
            <!-- .navbar -->
            <nav class="navbar navbar-inverse navbar-static-top">
                <div class="container-fluid">


                    <!-- Brand and toggle get grouped for better mobile display -->
                    <header class="navbar-header">

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>

                    </header>



                    <div class="topnav">
                        <div class="btn-group">
                            <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip"
                            class="btn btn-default btn-sm" id="toggleFullScreen">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a data-placement="bottom" data-original-title="Invoice" href="{{ url('/home/invoice') }}"data-toggle="tooltip"
                        class="btn btn-default btn-sm">
                        <i class="fa fa-credit-card"></i>
                        <span class="label label-warning">{{ $variable1 }}</span>
                    </a>
                    <a data-placement="bottom" data-original-title="After Sale" href="{{ url('/home/aftersalereview') }}" data-toggle="tooltip"
                    class="btn btn-default btn-sm">
                    <i class="fa fa-wrench"></i>
                    <span class="label label-danger">{{ $variable2 }}</span>
                </a>
        </div>
        <div class="btn-group">
            <a href="{{ route('logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm">
            <i class="fa fa-fw fa-power-off"></i>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </div>
    <div class="btn-group">
        <a data-placement="bottom" data-original-title="Show / Hide Left" data-toggle="tooltip"
        class="btn btn-primary btn-sm toggle-left" id="menu-toggle">
        <i class="fa fa-bars"></i>
    </a>
    <a href="#right" data-toggle="onoffcanvas" class="btn btn-default btn-sm" aria-expanded="false">
        <span class="fa fa-fw fa-comment"></span>
    </a>
</div>

</div>


</div>
<!-- /.container-fluid -->
</nav>
<!-- /.navbar -->
@yield('heading')

</div>
<!-- /#top -->

<!-- /#left -->
<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
            <div id="page-wrapper">

                <div class="container-fluid">


                   @yield('content')
               </div>
               <!-- /.container-fluid -->

           </div>
           <!-- /#page-wrapper -->


       </div>
       <!-- /.inner -->
   </div>
   <!-- /.outer -->
</div>
<!-- /#content -->

<div id="right" class="onoffcanvas is-right is-fixed bg-light" aria-expanded=false>
    <a class="onoffcanvas-toggler" href="#right" data-toggle=onoffcanvas aria-expanded=false></a>
    <br>
    <br>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Warning!</strong> Best check yo self, you're not looking too good.
    </div>
    <!-- .well well-small -->
    <div class="well well-small dark">
        <ul class="list-unstyled">
            <li>Visitor <span class="inlinesparkline pull-right">1,4,4,7,5,9,10</span></li>
            <li>Online Visitor <span class="dynamicsparkline pull-right">Loading..</span></li>
            <li>Popularity <span class="dynamicbar pull-right">Loading..</span></li>
            <li>New Users <span class="inlinebar pull-right">1,3,4,5,3,5</span></li>
        </ul>
    </div>
    <!-- /.well well-small -->
    <!-- .well well-small -->
    <div class="well well-small dark">
        <button class="btn btn-block">Default</button>
        <button class="btn btn-primary btn-block">Primary</button>
        <button class="btn btn-info btn-block">Info</button>
        <button class="btn btn-success btn-block">Success</button>
        <button class="btn btn-danger btn-block">Danger</button>
        <button class="btn btn-warning btn-block">Warning</button>
        <button class="btn btn-inverse btn-block">Inverse</button>
        <button class="btn btn-metis-1 btn-block">btn-metis-1</button>
        <button class="btn btn-metis-2 btn-block">btn-metis-2</button>
        <button class="btn btn-metis-3 btn-block">btn-metis-3</button>
        <button class="btn btn-metis-4 btn-block">btn-metis-4</button>
        <button class="btn btn-metis-5 btn-block">btn-metis-5</button>
        <button class="btn btn-metis-6 btn-block">btn-metis-6</button>
    </div>
    <!-- /.well well-small -->
    <!-- .well well-small -->
    <div class="well well-small dark">
        <span>Default</span><span class="pull-right"><small>20%</small></span>

        <div class="progress xs">
            <div class="progress-bar progress-bar-info" style="width: 20%"></div>
        </div>
        <span>Success</span><span class="pull-right"><small>40%</small></span>

        <div class="progress xs">
            <div class="progress-bar progress-bar-success" style="width: 40%"></div>
        </div>
        <span>warning</span><span class="pull-right"><small>60%</small></span>

        <div class="progress xs">
            <div class="progress-bar progress-bar-warning" style="width: 60%"></div>
        </div>
        <span>Danger</span><span class="pull-right"><small>80%</small></span>

        <div class="progress xs">
            <div class="progress-bar progress-bar-danger" style="width: 80%"></div>
        </div>
    </div>
</div>
<!-- /#right -->
</div>
<!-- /#wrap -->
<footer class="Footer bg-dark dker">
    <p>2017 &copy; Metis Bootstrap Admin Template v2.4.2</p>
</footer>
<!-- /#footer -->
<!-- #helpModal -->
<div id="helpModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- /#helpModal -->
<!--jQuery -->
<script src="{{ asset('marketing-template1/lib/jquery/jquery.js') }}"></script>

<script>
$(document).ready(function(){
	var maxField = 999; //Input fields increment limitation
	var addButton = $('.add_button'); //Add button selector
	var wrapper = $('.field_wrapper'); //Input field wrapper
	var fieldHTML = '<div><input type="text" placeholder="Item" name="field_name[]" value="" required/>&nbsp;<input type="number" placeholder="Price" name="field_price[]" value="" required/>&nbsp;<a href="javascript:void(0);" class="remove_button btn btn-danger btn-sm" title="Remove field"><i class="fa fa-trash"></i></a></div>'; //New input field html
	var x = 1; //Initial field counter is 1

	$(addButton).click(function(){ //Once add button is clicked
		if(x < maxField){ //Check maximum number of input fields
			x++; //Increment field counter
			$(wrapper).append(fieldHTML); // Add field html
		}
	});
	$(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
		e.preventDefault();
		$(this).parent('div').remove(); //Remove field html
		x--; //Decrement field counter
	});
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.5.1/prism.min.js"></script>

<!--Bootstrap -->
<script src="{{ asset('marketing-template1/lib/bootstrap/js/bootstrap.js') }}"></script>
<!-- MetisMenu -->
<script src="{{ asset('marketing-template1/lib/metismenu/metisMenu.js') }}"></script>
<!-- onoffcanvas -->
<script src="{{ asset('marketing-template1/lib/onoffcanvas/onoffcanvas.js') }}"></script>
<!-- Screenfull -->
<script src="{{ asset('marketing-template1/lib/screenfull/screenfull.js') }}"></script>

<!-- Metis core scripts -->
<script src="{{ asset('marketing-template1/js/core.js') }}"></script>
<!-- Metis demo scripts -->
<script src="{{ asset('marketing-template1/js/app.js') }}"></script>

</body>

</html>
