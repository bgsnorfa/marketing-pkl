<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Lokavor Studio</title>

    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">

    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />

    <link href="{{ asset('marketing-template/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('marketing-template/css/sb-admin.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('marketing-template/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">


    <!-- Bootstrap -->
    <link href="{{ asset('marketing-template1/lib/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Metis core stylesheet -->
    <link href="{{ asset('marketing-template1/css/main.css') }}" rel="stylesheet">

    <!-- metisMenu stylesheet -->
    <link href="{{ asset('marketing-template1/lib/metismenu/metisMenu.css') }}" rel="stylesheet">

    <!-- onoffcanvas stylesheet -->
    <link href="{{ asset('marketing-template1/lib/onoffcanvas/onoffcanvas.css') }}" rel="stylesheet">

    <!-- animate.css stylesheet -->
    <link href="{{ asset('marketing-template1/lib/animate.css/animate.css') }}" rel="stylesheet">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.5.1/themes/prism.min.css">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->



<link href="{{ asset('marketing-template1/less/theme.less') }}" rel="stylesheet/less">
<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script>

</head>

<body>
    <div class="bg-dark dk" id="wrap">
        <div id="top">
            <!-- .navbar -->
            <nav class="navbar navbar-inverse navbar-static-top">
                <div class="container-fluid">


                    <!-- Brand and toggle get grouped for better mobile display -->
                    <header class="navbar-header">

                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>

                    </header>



                    <div class="topnav">
                        <div class="btn-group">
                            <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip"
                            class="btn btn-default btn-sm" id="toggleFullScreen">
                            <i class="glyphicon glyphicon-fullscreen"></i>
                        </a>
                    </div>
                    <div class="btn-group">
                        <a data-placement="bottom" data-original-title="Invoice" href="{{ url('/finance/invoice') }}"data-toggle="tooltip"
                        class="btn btn-default btn-sm">
                        <i class="fa fa-credit-card"></i>
                        <span class="label label-warning">{{ $variable1 }}</span>
                    </a>
      </div>
        <div class="btn-group">
            <a href="{{ route('logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm">
            <i class="fa fa-fw fa-power-off"></i>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </div>

</div>


</div>
<!-- /.container-fluid -->
</nav>
<!-- /.navbar -->
@yield('heading')

</div>
<!-- /#top -->
<div id="left">
    <div class="media user-media bg-dark dker">
        <div class="user-media-toggleHover">
            <span class="fa fa-user"></span>
        </div>
        <div class="user-wrapper bg-dark">
            <a class="user-link" href="">
                <img class="media-object img-thumbnail user-img" alt="User Picture" src="{{ URL::asset('images/L.png')}}" width="75px" height="75px">
               </a>

            <div class="media-body">
                <h5 class="media-heading">{{ Auth::user()->name}}</h5>
                <ul class="list-unstyled user-info">
                    <li>{{ Auth::user()->role}}</li>
                    <li>Member Since : <br>
                        <small><i class="fa fa-calendar"></i>&nbsp;{{ Auth::user()->created_at}}</small>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #menu -->
    <style>
        #menu {
          background-color: #333 !important;
      }
  </style>
  <ul id="menu" >
    <li class="nav-header">Menu</li>
    <li class="nav-divider"></li>

    <li class="{{ ($menu == 'index') ? 'active' : '' }}">
       <a href="{{ url('finance') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
   </li>

   <!-- <li class="{{ ($menu == 'consument') ? 'active' : '' }}">
       <a href="javascript:;"><i class="fa fa-user "></i>
          <span class="link-title">Consument</span>
          <span class="fa arrow"></span>
      </a>
      <ul id="consument" class="collapse">
         <li>
            <a href="{{ url('/home/consument') }}">Browse Consuments</a>
        </li>
        <li>
            <a href="{{ url('/home/consument/add') }}">Add Consument</a>
        </li>
    </ul>
</li> -->

<!-- <li class="{{ ($menu == 'quotation') ? 'active' : '' }}">
   <a href="javascript:;"><i class="fa fa-file "></i>
      <span class="link-title">Quotation</span>
      <span class="fa arrow"></span>
  </a>
  <ul id="quotation" class="collapse">
      <li>
         <a href="{{ url('/home/quotation') }}">Browse Quotations</a>
     </li>
     <li>
         <a href="{{ url('/home/quotation/draft') }}">Browse Quotations Draft</a>
     </li>
     <li>
         <a href="{{ url('/home/quotation/add') }}">Add new Quotation</a>
     </li>
 </ul>
</li> -->


<!-- <li class="{{ ($menu == 'contract') ? 'active' : '' }}">
   <a href="javascript:;"><i class="fa fa-book "></i>
      <span class="link-title">Contract</span>
      <span class="fa arrow"></span>
  </a>
  <ul id="contract" class="collapse">
      <li>
         <a href="{{ url('/home/contract') }}">Browse Contracts</a>
     </li>
     <li>
         <a href="{{ url('/home/contract/add') }}">Add new Contract</a>
     </li>
 </ul>
</li> -->

<li class="{{ ($menu == 'invoice') ? 'active' : '' }}">
   <a href="javascript:;"><i class="fa fa-credit-card "></i>
      <span class="link-title">Invoice</span>
      <span class="fa arrow"></span>
  </a>
  <ul id="invoice" class="collapse">
      <li>
         <a href="{{ url('/finance/invoice') }}">Browse Invoices</a>
     </li>
     <li>
         <a href="{{ url('/finance/invoice/view') }}">Add Invoices</a>
     </li>
 </ul>
</li>

<!-- <li class="{{ ($menu == 'aftersale') ? 'active' : '' }}">
    <a href="javascript:;"><i class="fa fa-wrench "></i>
      <span class="link-title">After Sale</span>
      <span class="fa arrow"></span>
  </a>
  <ul id="invoice" class="collapse">
      <li>
         <a href="{{ url('/home/aftersale') }}">Browse After Sales</a>
     </li>
     <li>
         <a href="{{ url('/home/aftersale/add') }}">Add new After Sale</a>
     </li>
 </ul>
</li> -->
<!-- /#menu -->
</ul>
</div>
<!-- /#left -->
<div id="content">
    <div class="outer">
        <div class="inner bg-light lter">
            <div id="page-wrapper">

                <div class="container-fluid">


                   @yield('content')
               </div>
               <!-- /.container-fluid -->

           </div>
           <!-- /#page-wrapper -->


       </div>
       <!-- /.inner -->
   </div>
   <!-- /.outer -->
</div>
<!-- /#content -->


</div>
<!-- /#wrap -->
<footer class="Footer bg-dark dker">
    <p>Copyright &copy;Lokavor Studio 2017</p>
</footer>
<!-- /#footer -->
<!--jQuery -->
<script src="{{ asset('marketing-template1/lib/jquery/jquery.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.5.1/prism.min.js"></script>

<!--Bootstrap -->
<script src="{{ asset('marketing-template1/lib/bootstrap/js/bootstrap.js') }}"></script>
<!-- MetisMenu -->
<script src="{{ asset('marketing-template1/lib/metismenu/metisMenu.js') }}"></script>
<!-- onoffcanvas -->
<script src="{{ asset('marketing-template1/lib/onoffcanvas/onoffcanvas.js') }}"></script>
<!-- Screenfull -->
<script src="{{ asset('marketing-template1/lib/screenfull/screenfull.js') }}"></script>

<!-- Metis core scripts -->
<script src="{{ asset('marketing-template1/js/core.js') }}"></script>
<!-- Metis demo scripts -->
<script src="{{ asset('marketing-template1/js/app.js') }}"></script>


</body>

</html>
