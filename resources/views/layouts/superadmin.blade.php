<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>Director Marketing</title>

	    <!-- Bootstrap -->

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('admin-template/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <!-- Custom fonts for this template -->
    <link href="{{ asset('admin-template/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="{{ asset('admin-template/vendor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('admin-template/css/sb-admin.css') }}" rel="stylesheet">

  </head>

  <body class="fixed-nav sticky-footer bg-dark" id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <a class="navbar-brand" href="{{ url('superadmin') }}">Director Marketing</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseConsument" data-parent="#consumentAccordion">
              <i class="fa fa-fw fa-user"></i>
              <span class="nav-link-text">
                Consumer</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseConsument">
                <li>
                    <a href="{{ url('superadmin/consument') }}">Consumer</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/consument/export') }}">Export</a>
                </li>
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseQuotation" data-parent="#quotationAccordion">
              <i class="fa fa-fw fa-file"></i>
              <span class="nav-link-text">
                Quotation</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseQuotation">
                <li>
                    <a href="{{ url('superadmin/quotation') }}">Quotation</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/quotation/draft') }}">Quotation Draft</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/quotation/export') }}">Export</a>
                </li>
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseContract" data-parent="#contractAccordion">
              <i class="fa fa-fw fa-book"></i>
              <span class="nav-link-text">
                Contract</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseContract">
                <li>
                    <a href="{{ url('superadmin/contract') }}">Contract</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/contract/export') }}">Export</a>
                </li>
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseInvoice" data-parent="#InvoiceAccordion">
              <i class="fa fa-fw fa-credit-card"></i>
              <span class="nav-link-text">
                Invoice</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseInvoice">
                <li>
                    <a href="{{ url('superadmin/invoice') }}">Invoice</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/invoice/export') }}">Export</a>
                </li>
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseAfterSale" data-parent="#afterSaleAccordion">
              <i class="fa fa-fw fa-wrench"></i>
              <span class="nav-link-text">
                AfterSale</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseAfterSale">
                <li>
                    <a href="{{ url('superadmin/aftersale') }}">AfterSale</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/aftersale/export') }}">Export</a>
                </li>
            </ul>
          </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUser" data-parent="#userAccordion">
              <i class="fa fa-fw fa-user-o"></i>
              <span class="nav-link-text">
                User</span>
            </a>
            <ul class="sidenav-second-level collapse" id="collapseUser">
                <li>
                    <a href="{{ url('superadmin/user') }}">Add User</a>
                </li>
                <li>
                    <a href="{{ url('superadmin/user/view') }}">User Data</a>
                </li>
            </ul>
          </li>

        </ul>

        <ul class="navbar-nav sidenav-toggler">
          <li class="nav-item">
            <a class="nav-link text-center" id="sidenavToggler">
              <i class="fa fa-fw fa-angle-left"></i>
            </a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">

          <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
              <i class="fa fa-fw fa-sign-out"></i>
              Logout</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            @yield('breadcrumbs')

        </ol>

        @yield('content')

      </div>
      <!-- /.container-fluid -->

    </div>
    <!-- /.content-wrapper -->

    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright &copy; Lokavor Studio 2017</small>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button -->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>

    <!-- Logout Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Select "Logout" below if you are ready to end your current session.
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                Logout</a>
          </div>
        </div>
      </div>
    </div>
	<!--Bootstrap -->
    <!-- <script src="{{ asset('marketing-template1/lib/bootstrap/js/bootstrap.js') }}"></script> -->

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('admin-template/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('admin-template/vendor/popper/popper.min.js') }}"></script>
    <script src="{{ asset('admin-template/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('admin-template/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('admin-template/vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('admin-template/vendor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin-template/vendor/datatables/dataTables.bootstrap4.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('admin-template/js/sb-admin.min.js') }}"></script>

    <script>
    $('#consument_year').on('change', function(e){
        console.log(e);
        var year = e.target.value;

        $.get('{{ url('superadmin') }}/consument/ajax?year=' + year, function(data) {
            console.log(data);
            $('#consument_month').empty();
            $.each(data, function(index,subCatObj){
                $('#consument_month').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });
    </script>

    <script>
    // invoices month
    $('#inv_year').on('change', function(e){
        console.log(e);
        var year = e.target.value;

        $.get('{{ url('superadmin') }}/invoice/ajaxmonth?year=' + year, function(data) {
            console.log(data);
            $('#inv_month').empty();
            $.each(data, function(index,subCatObj){
                $('#inv_month').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });
    </script>

    <script>
    // quotation export script
    $('#quotation_marketing').on('change', function(e){
        console.log(e);
        var marketing = e.target.value;

        $.get('{{ url('superadmin') }}/quotation/ajaxyear?marketing=' + marketing, function(data) {
            console.log(data);
            $('#quotation_year').empty();
            $('#quotation_month').empty();
            $('#quotation_year').append('<option value="">---Select Year---</option>');
            $('#quotation_month').append('<option value="">---Select Year---</option>');
            $.each(data, function(index,subCatObj){
                $('#quotation_year').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });

    $('#quotation_year').on('change', function(e){
        console.log(e);
        var year = e.target.value;
        var marketing = $('#quotation_marketing').val();
        console.log(marketing);

        $.get('{{ url('superadmin') }}/quotation/ajaxmonth?year=' + year + '&marketing=' + marketing, function(data) {
            console.log(data);
            $('#quotation_month').empty();
            $('#quotation_month').append('<option value="">---Select Year---</option>');
            $.each(data, function(index,subCatObj){
                $('#quotation_month').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });
    </script>

    <script>
    // contract export script
    $('#contract_marketing').on('change', function(e){
        console.log(e);
        var marketing = e.target.value;

        $.get('{{ url('superadmin') }}/contract/ajaxyear?marketing=' + marketing, function(data) {
            console.log(data);
            $('#contract_year').empty();
            $('#contract_month').empty();
            $('#contract_year').append('<option value="">---Select Year---</option>');
            $('#contract_month').append('<option value="">---Select Month---</option>');
            $.each(data, function(index,subCatObj){
                $('#contract_year').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });

    $('#contract_year').on('change', function(e){
        console.log(e);
        var year = e.target.value;
        var marketing = $('#contract_marketing').val();
        console.log(marketing);

        $.get('{{ url('superadmin') }}/contract/ajaxmonth?year=' + year + '&marketing=' + marketing, function(data) {
            console.log(data);
            $('#contract_month').empty();
            $('#contract_month').append('<option value="">---Select Month---</option>');
            $.each(data, function(index,subCatObj){
                $('#contract_month').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });
    </script>

    <script>
    // aftersales export script
    $('#as_marketing').on('change', function(e){
        console.log(e);
        var marketing = e.target.value;

        $.get('{{ url('superadmin') }}/aftersale/ajaxyear?marketing=' + marketing, function(data) {
            console.log(data);
            $('#as_year').empty();
            $('#as_month').empty();
            $('#as_year').append('<option value="">---Select Year---</option>');
            $('#as_month').append('<option value="">---Select Month---</option>');
            $.each(data, function(index,subCatObj){
                $('#as_year').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });

    $('#as_year').on('change', function(e){
        console.log(e);
        var year = e.target.value;
        var marketing = $('#as_marketing').val();
        console.log(marketing);

        $.get('{{ url('superadmin') }}/aftersale/ajaxmonth?year=' + year + '&marketing=' + marketing, function(data) {
            console.log(data);
            $('#as_month').empty();
            $('#as_month').append('<option value="">---Select Month---</option>');
            $.each(data, function(index,subCatObj){
                $('#as_month').append('<option value="'+subCatObj+'">'+subCatObj+'</option>');
            });
        });
    });
    </script>

  </body>

</html>
