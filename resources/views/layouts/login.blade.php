<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/grayscale.min.css') }}" rel="stylesheet">
    <style>

        .panel-transparent {
            background: none;
        }

        .panel-transparent .panel-heading{
            background: rgba(122, 130, 136, 0.2)!important;
        }

        .panel-transparent .panel-body{
            background: rgba(46, 51, 56, 0.2)!important;
        }
        .x {
           font-family: "Rockwell ", "Rockwell Bold", monospace;
           
			
       </style>
   </head>
   <body class="download-section">
      
    @yield('content')
    

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ URL::asset('js/grayscale.min.js') }}"></script>
</body>
</html>
