@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Consumer</h3>
    </div>
                            <!-- /.main-bar -->
        </header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Consumer <small>Add new Consumer</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/consument') }}"><i class="fa fa-user"></i> Consumer</a>
            </li>
            <li class="active">
                Add
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/home/consument/add') }}">

            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label>Name</label>
                <input type="text" name="name" class="form-control" placeholder="consumer name" value="{{ old('name') }}">
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label>Email</label>
                <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="consumer email address">
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                <label>Phone</label>
                <input type="number" name="phone" class="form-control" placeholder="consumer phone number" value="{{ old('phone') }}">
                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                <label>Address</label>
                <textarea class="form-control" name="address" placeholder="consumer address">{{ old('address') }}</textarea>
                {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
            </div>
            <input type="submit" name="save" value="Save" class="btn btn-md btn-success">
        </form>
    </div>
</div>
@endsection
