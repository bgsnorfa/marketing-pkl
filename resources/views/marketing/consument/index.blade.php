@extends('layouts.marketing')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
				<input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
						<button class="btn btn-primary btn-sm text-muted" type="button">
							<i class="fa fa-search"></i>
						</button>
                    </span>
                </div>	
        </div>		
                                    <!-- /.main-search -->                             
	</div>
                               
    <div class="main-bar">
		<h3><i class="fa fa-user"></i>&nbsp;Consument</h3>
	</div>
                            <!-- /.main-bar -->
		</header>
                        <!-- /.head -->

@endsection

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Consumer <small>Browse Consumer</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-user"></i> Consumer
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">

<div class="col-lg-12">
     
	 <table class="table table-condensed table-bordered table-hover table-responsive ajaxdata" style="display:none">
		<thead>
		   <tr>
               
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
				<th>Phone</th> 
                <th>Address</th> 
                <th>Status</th> 
                <th>Action</th>
          
            </tr>
		</thead>
       
            
        <tbody id="success">
		</tbody>
     </table>
	 
	 <table class="table table-bordered table-condensed table-hover table-responsive generaldata">
		<thead>
		   <tr>
               
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
				<th>Phone</th> 
                <th>Address</th> 
                <th>Status</th> 
                <th>Action</th>
          
            </tr>
		</thead>
            
        <tbody>
		<?php $count = 1; ?> 
            @foreach($consuments as $data) 
            <tr> 
                <td>{{ $count }}</td> 
                <td>{{ $data->name }}</td> 
                <td>{{ $data->email }}</td> 
                <td>{{ $data->phone }}</td> 
                <td>{{ $data->address }}</td> 
                 @if($data->status ==='contacted')
				 <td ><span class="label label-success label-block label1"><b>Contacted</b></span></td>               
				@else
				<td><span class="label label-default label-block label1"><b>Prospecting</b></span></td>
				@endif
                <td> 
                    <a data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/home/consument/' . $data->id . '/edit') }}" class="btn btn-xs btn-default"><i class="fa fa-fw fa-pencil"></i></a> 
 
                    @if($data->status === 'prospecting') 
                        <a data-placement="top" data-original-title="Contact" data-toggle="tooltip" href="{{ url('/home/consument/' .$data->id. '/contact') }}" class="btn btn-xs btn-metis-6"><i class="fa fa-fw fa-phone"></i></a> 
                    @endif 
 
                    @if($data->status === 'contacted') 
                        <a data-placement="top" data-original-title="Create Quotation" data-toggle="tooltip" href="{{ url('/home/quotation/add/consument/' . $data->id) }}" class="btn btn-xs btn-success"><i class="fa fa-fw fa-file"></i></a> 
                    @endif 
                </td> 
            </tr> 
            <?php $count++; ?> 
            @endforeach 
		</tbody>
     </table> 
    </div>
</div>


<script type="text/javascript">
	function search(){
		var search =$('#search').val();
		if(search){
			$(".generaldata").hide();
			$(".ajaxdata").show();
		}else{
			$(".generaldata").show();
			$(".ajaxdata").hide();
		}
		$.ajax({
			type: "POST",
			url: '{{URL::to("/home/consument/search")}}',
			data: {
				search: search,
				_token: $('#signup-token').val()	
			},
			datatype:'html',
			
			success: function(response){
				console.log(response);
				$("#success").html(response);
			}
		});
	}
</script>
@endsection
