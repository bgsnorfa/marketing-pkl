@extends('layouts.marketing')
@section('heading')
<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-home"></i>&nbsp;Dashboard</h3>
    </div>
                            <!-- /.main-bar -->
        </header>
  <!-- /.head -->
  @endsection
  @section('content')

  <!-- Page Heading -->
  <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Dashboard <small>Statistics Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<style>
table { border-collapse: collapse; }
tr { border: none; }
td {
  border-right: solid 3px #FFF; 
  border-left: none;
}	
</style>
<!-- /.row -->

<div class="row">
    <div class="col-lg-12 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-1">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-xs-11 text-right">
                    <div class="huge"></div>
                    <div>Consumer</div>
                </div>
                    <div class="col-xs-10 text-center">
                     <div>
                        <table>
                           <tr>
                              <td class=" col-xs-3"> <p class="huge"> {{$consument}}</p></td> 
                              <td class=" col-xs-3"> <p class="huge"> {{$consument1}}</p></td> 
                              <td class=" col-xs-3"> <p class="huge"> {{$consument2}}</p></td> 
                          </tr>
                          <tr>
                              <td class="col-xs-3"> <p> All Consumer</p></td> 
                              <td class="col-xs-3"> <p> Consumer With Quo</p></td> 
                              <td class="col-xs-3"> <p> Consumer With Con</p></td> 
                          </tr>
                      </table>
                  </div>
              </div>
          </div>
      </div>
      <a href="{{ url('home/consument') }}">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
        </div>
    </a>
</div>
</div>

<?php $value =0;?>
@foreach($quotation as $quo)
@if($quo->status === 'accepted')
<?php $value = $value + $quo->price;?>
@endif	
@endforeach


<div class="col-lg-12 col-md-6">
    <div class="panel panel-green">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="fa fa-file fa-5x"></i>
                </div>
                <div class="col-xs-11 text-right">
                    <div class="huge"></div>
                    <div>Quotation</div>
                </div>
                <div class="col-xs-10 text-center">
                  <div>
                      <table>
                         <tr>
                            <td class=" col-xs-3"> <p class="huge"> {{$quotation1}}</p></td> 
                            <td class=" col-xs-3"> <p class="huge"> {{$quotation2}}</p></td> 
                            <td class=" col-xs-3"> <p class="huge"> {{$quotation3}}</p></td> 
                            <td class=" col-xs-3"> <p class="huge"> {{$quotation4}}</p></td> 
                            <td class=" col-xs-6"> <p class="huge"> {{ number_format($value) }}</p></td> 
                            
                        </tr>
                        <tr>
                            <td class="col-xs-3"> <p> All Quotations</p></td> 
                            <td class="col-xs-3"> <p> Quotations On-Process</p></td> 
                            <td class="col-xs-3"> <p> Quotations Accepted</p></td> 
                            <td class="col-xs-3"> <p> Quotations Rejected</p></td> 
                            <td class="col-xs-6"> <p>  Value(IDR)</p></td> 
                        </tr>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
    <a href="{{ url('home/quotation') }}">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
        </div>
    </a>
</div>
</div>


<div class="col-lg-12 col-md-6">
    <div class="panel panel-yellow">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="fa fa-book fa-5x"></i>
                </div>
                <div class="col-xs-11 text-right">
                    <div class="huge"></div>
                    <div>Contract</div>
                </div>
                <div class="col-xs-3 text-center">
                    <div>
                        <table>
                           <tr>
                              <td class=" col-xs-3"> <p class="huge"> {{$contract}}</p></td> 
                             </tr>
                          <tr>
                              <td class="col-xs-3"> <p> All Contract</p></td> 
                             </tr>
                      </table>
                  </div>
              </div>
          </div>
      </div>
      <a href="{{ url('home/contract') }}">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
        </div>
    </a>
</div>
</div>
<div class="col-lg-12	 col-md-6">
    <div class="panel panel-red">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="fa fa-credit-card fa-5x"></i>
                </div>
                <div class="col-xs-11 text-right">
                    <div class="huge"></div>
                    <div>Invoice</div>
                </div>
                <div class="col-xs-10 text-center">
                  <div>
                      <table>
                         <tr>
                            <?php $paid = 0;?>
                            <?php $total = 0;?>
                            <?php $paidvalue = 0;?>
                            <?php $unpaid = 0;?>
                            @foreach($invoice as $inv)
                            @if($inv->price === $inv->paid)
                            <?php $paid = $paid + 1;?>
                            @else
                            <?php $unpaid = $unpaid+1;?>
                            @endif	
                            @endforeach
                            
                            @foreach($invoice as $inv)
                            <?php $paidvalue = $paidvalue + $inv->paid;?>
                            <?php $total = $total + $inv->price;?>
                            @endforeach
                            
                            <td class=" col-xs-3"> <p class="huge"> {{$paid}}</p></td> 
                            <td class=" col-xs-3"> <p class="huge"> {{$unpaid}}</p></td> 
                            <td class=" col-xs-3"> <p class="huge"> {{ number_format($total) }}</p></td> 
                            <td class=" col-xs-3"> <p class="huge"> {{ number_format($paidvalue) }}</p></td> 
                            
                        </tr>
                        <tr>
                            <td class="col-xs-3"> <p>Paid Quotations</p></td> 
                            <td class="col-xs-3"> <p>Unpaid Quotations</p></td> 
                            <td class="col-xs-3"> <p> Total Value(IDR)</p></td> 
                            <td class="col-xs-3"> <p> Paid Value(IDR)</p></td> 
                        </tr>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
    <a href="{{ url('home/invoice') }}">
        <div class="panel-footer">
            <span class="pull-left">View Details</span>
            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
            <div class="clearfix"></div>
        </div>
    </a>
</div>
</div>

<div class="col-lg-12	 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-1">
                    <i class="fa fa-wrench fa-5x"></i>
                </div>
                <div class="col-xs-11 text-right">
                    <div class="huge"></div>
                    <div>After Sale</div>
                </div>
                <div class="col-xs-10 text-center">
                    <div>
                        <table>
                           <tr>
                              <td class=" col-xs-3"> <p class="huge"> {{$aftersale}}</p></td> 
                              <td class=" col-xs-3"> <p class="huge"> {{$aftersale1}}</p></td> 
                              <td class=" col-xs-3"> <p class="huge"> {{$aftersale2}}</p></td> 
                             </tr>
                          <tr>
                              <td class="col-xs-3"> <p> All After Sale</p></td> 
                              <td class="col-xs-3"> <p> Active </p></td> 
                              <td class="col-xs-3"> <p> Nearly Ended </p></td> 
                             </tr>
                      </table>
                  </div>
              </div>
          </div>
            
        </div>
        <a href="{{ url('admin/aftersale') }}">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
    </div>
</div>
</div>

<!-- /.row -->



@endsection
