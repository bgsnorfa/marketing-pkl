@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm text-muted" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>  
        </div>      
        <!-- /.main-search -->                             
    </div>

    <div class="main-bar">
            <h3><i class="fa fa-book"></i>&nbsp;Contract</h3>
        </div>
                            <!-- /.main-bar -->
        </header>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Browse Contracts</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-book"></i> Contract
            </li>
        </ol>
    </div>
</div>
<div class="row">
  <div class="col-lg-12">

      <table class="table table-bordered table-condensed table-hover table-responsive ajaxdata" style="display:none">
          <thead>
           <tr>
             <th>No</th>
             <th>Quotation no</th>
             <th>Contract nanme</th>
             <th>Contract no</th>
             <th>Description</th>
             <th>Action</th>
         </tr>
     </thead>


     <tbody id="success">
     </tbody>
 </table>
 <table class="table table-bordered table-condensed table-hover table-responsive generaldata">
  <thead>
      <tr>
        <th>No</th>
        <th>Quotation no</th>
        <th>Contract name</th>
        <th>Contract no</th>
        <th>Description</th>
        <th>Action</th>
    </tr>
</thead>

<tbody>
   <?php $count = 1; ?>
   @foreach($contracts as $data)

   <tr>
    <td>{{ $count }}</td>
    <td>{{ $data->quo }}</td>
    <td>{{ $data->name}}</td>
    <td>{{ $data->con }}</td>
    <td>{{ $data->desc }}</td>
    <td>
	   <a href="{{ url('/home/contract/' . $data->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-info"></i></a>
	 <a href="{{ url('/home/contract/' . $data->id . '/edit') }}" class="btn btn-default btn-xs"><i class="fa fa-fw fa-pencil"></i></a>
      <a href="{{ url('/contracts/' . $data->file) }}" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-file-pdf-o"></i></a>
  </td>
</tr>
<?php $count = $count+1; ?>
@endforeach
</tbody>
</table>
</div>
</div>

<script type="text/javascript">
    function search(){
        var search =$('#search').val();
        if(search){
            $(".generaldata").hide();
            $(".ajaxdata").show();
        }else{
            $(".generaldata").show();
            $(".ajaxdata").hide();
        }
        $.ajax({
            type: "POST",
            url: '{{URL::to("/home/contract/search")}}',
            data: {
                search: search,
                _token: $('#signup-token').val()    
            },
            datatype:'html',
            
            success: function(response){
                console.log(response);
                $("#success").html(response);
            }
        });
    }
</script>
@endsection
