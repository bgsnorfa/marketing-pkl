@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-book"></i>&nbsp;Contract</h3>
    </div>
                            <!-- /.main-bar -->
        </header>
                        <!-- /.head -->
<!-- /.row -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Edit Existing Contract</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('home/contract') }}"><i class="fa fa-book"></i> Contract</a>
            </li>
            <li class="active">
                Edit
            </li>
        </ol>
    </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">

      {{ csrf_field() }}

      <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        <label>Contract name</label>
        <input type="text" name="name" value="{{ $contract->name }}" placeholder="Contract name" class="form-control">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>

      <div class="form-group {{ $errors->has('desc') ? 'has-error' : '' }}">
        <label>Contract description</label>
        <textarea name="desc" placeholder="Contract description" class="form-control">{{ $contract->desc }}</textarea>
        {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
      </div>

      <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
        <label>Upload new Contract file (ignore if don't want to change contract file)</label>
        <br/>
        <a class="btn btn-xs btn-primary" href="{{ url( 'contracts/' . $contract->file ) }}"><i class="fa fa-fw fa-folder-open"></i> View current Contract file</a>
        <input type="file" name="file" class="form-control">
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
      </div>

      <input type="submit" name="update" class="btn btn-success btn-md" value="Update">

    </form>
  </div>
</div>
@endsection
