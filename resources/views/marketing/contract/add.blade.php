@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-book"></i>&nbsp;Contract</h3>
    </div>
                            <!-- /.main-bar -->
        </header>
                        <!-- /.head -->
<!-- /.row -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>Add new Contract</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/contract') }}"><i class="fa fa-book"></i> Contract</a>
            </li>
            <li class="active">
              Add
            </li>
        </ol>
    </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">

      {{ csrf_field() }}

      <div class="form-group {{ $errors->has('quo') ? 'has-error' : '' }}">
        <label>Select Quotation</label>
        <select class="form-control" name="quo" required="required">
          <option value="">--- Select Quotation ---</option>
          @foreach($quotation as $data)
          <option value={{ $data->quo }}>{{ $data->quo }} - {{ $data->project }}</option>
          @endforeach
        </select>
        {!! $errors->first('quo', '<p class="help-block">:message</p>') !!}
      </div>

      <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        <label>Contract name</label>
        <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Contract name">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>

      <div class="form-group {{ $errors->has('desc') ? 'has-error'  : '' }}">
        <label>Description</label>
        <textarea class="form-control" name="desc" placeholder="Contract description">{{ old('desc') }}</textarea>
        {!! $errors->first('desc', '<p class="help-block">:message</p>') !!}
      </div>

      <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
        <label>Description file</label>
        <input type="file" name="file" class="form-control" value="{{ old('file') }}">
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
      </div>

      <input type="submit" name="save" class="btn btn-md btn-success" value="Save">
    </form>
  </div>
</div>
@endsection
