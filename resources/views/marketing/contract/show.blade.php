@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Contract</h3>
    </div>
                            <!-- /.main-bar -->
        </header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Contract <small>{{ $contract->con }}</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/contract') }}"><i class="fa fa-file"></i> Contract</a>
            </li>
            <li class="active">
                {{ $contract->con }}
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="well">{{ $contract->name }}</h2>
        <h4>Contract Number = <i>{{ $contract->con }}</i></h4>
        <br/>
        <a href="{{ url('/home/contract') }}"><span class="glyphicon glyphicon-arrow-left"></span> Back to contract</a>
    </div>
</div>

<div class="row">
    <div clas="col-lg-12">
        <table class="table table-hover">
            <tr>
                <td>Quo Number</td>
                <td>{{ $contract->quo }}</td>
            </tr>
			 <tr>
                <td>Project</td>
                <td>{{ $contract->project }}</td>
            </tr>
			<tr>
                <td>Contract Description</td>
                <td>{{ $contract->desc }}</td>
            </tr>
			 <tr>
                <td>Created at</td>
                <td>{{ $contract->created_at }}</td>
            </tr>

            <tr>
                <td>Updated at</td>
                <td>{{ $contract->updated_at }}</td>
            </tr>
        </table>
    </div>
</div>
@endsection
