@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Quotation</h3>
    </div>
                            <!-- /.main-bar -->
        </header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>{{ $quotation->quo }}</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/quotation') }}"><i class="fa fa-file"></i> Quotation</a>
            </li>
            <li class="active">
                {{ $quotation->quo }}
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="well">{{ $quotation->project }}</h2>
        <h4>Quotation number = <i>{{ $quotation->quo }}</i></h4>
        <h4>Status = {{ $quotation->status }}</h4>
        <br/>
        <a href="{{ url('/home/quotation') }}"><span class="glyphicon glyphicon-arrow-left"></span> Back to Quotation</a>
        @if($quotation->status === 'waiting')
            <a class="btn btn-primary btn-xs" href="{{ url('/home/quotation/'.$quotation->id.'/edit') }}">Edit this quotation</a>
        @endif
    </div>
</div>

<div class="row">
    <div clas="col-lg-12">
        <table class="table table-hover">
            <tr>
                <td>Consument name</td>
                <td>{{ $quotation->name }}</td>
            </tr>

            <tr>
                <td>Consument email</td>
                <td>{{ $quotation->email }}</td>
            </tr>

            <tr>
                <td>Consument phone</td>
                <td>{{ $quotation->phone }}</td>
            </tr>

            <tr>
                <td>Consument address</td>
                <td>{{ $quotation->address }}</td>
            </tr>

            <tr>
                <td>Project time</td>
                <td>{{ $quotation->time }}</td>
            </tr>

            <tr>
                <td>Created at</td>
                <td>{{ $quotation->created_at }}</td>
            </tr>

            <tr>
                <td>Updated at</td>
                <td>{{ $quotation->updated_at }}</td>
            </tr>
        </table>

        <h3>Quotation Items</h3>
        <table class="table table-hover table-bordered">
            <tr>
                <th>No</th>
                <th>Item</th>
                <th>Price(IDR)</th>
            </tr>
            @foreach($items as $indexKey=>$a)
            <tr>
                <td>{{ $indexKey+1 }}</td>
                <td>{{ $a->item }}</td>
                <td align="right">{{ number_format($a->harga) }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
