@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Invoice</h3>
    </div>
    <!-- /.main-bar -->
</header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Accept Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/quotation') }}"><i class="fa fa-user"></i>Quotation</a>
            </li>
            <li class="active">
                Accept
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">


        {{ csrf_field() }}
        <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
            <label>Invoice File</label>
            <input type="file" name="file" class="form-control" value="{{ old('file') }}">
            {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
        </div>
        <input type="submit" name="save" value="Accept" class="btn btn-md btn-success">
    </form>
</div>
</div>
@endsection
