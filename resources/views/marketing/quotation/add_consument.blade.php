@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Quotation</h3>
    </div>
                            <!-- /.main-bar -->
        </header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Add Quotation from existing Consumer</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('home/quotation') }}"><i class="fa fa-file"></i> Quotation</a>
            </li>
            <li class="active">
                Add
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">

        <div class="well">
            <table class="table">
                <tr>
                    <td>Consumer name</td>
                    <td>{{ $consument->name }}</td>
                </tr>
                <tr>
                    <td>Consumer email</td>
                    <td>{{ $consument->email }}</td>
                </tr>
            </table>

        </div>

        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('project') ? 'has-error' : '' }}">
                <label>Project name</label>
                <input type="text" name="project" class="form-control" placeholder="Project name" value="{{ old('project') }}">
                {!! $errors->first('project', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('time') ? 'has-error' : '' }}">
                <label>Project time</label>
                <input type="text" name="time" class="form-control" placeholder="Project time" value="{{ old('time') }}">
                {!! $errors->first('time', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group field_wrapper">
                <label>Project Items <a href="javascript:void(0);" class="add_button btn btn-info btn-xs" title="Add field">Add items</a></label>
                <br/>
                <div>
                    <input placeholder="Item" type="text" name="field_name[]" value=""/>
                    <input type="number" placeholder="Price" name="field_price[]" value=""/>
                </div>
            </div>

            <input type="submit" name="save" class="btn btn-md btn-success" value="Create">
            <input type="submit" name="draft" class="btn btn-md btn-warning" value="Draft">
        </form>
    </div>
</div>
@endsection
