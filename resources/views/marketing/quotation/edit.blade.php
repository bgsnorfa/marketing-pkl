@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Quotation</h3>
    </div>
                            <!-- /.main-bar -->
        </header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Edit Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('home/quotation') }}"><i class="fa fa-file"></i> Quotation</a>
            </li>
            <li>
                Edit
            </li>
            <li class="active">
                {{ $quotation->quo }}
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('project') ? 'has-error' : '' }}">
                <label>Project name</label>
                <input type="text" name="project" class="form-control" placeholder="Project name" value="{{ $quotation->project }}">
                {!! $errors->first('project', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('time') ? 'has-error' : '' }}">
                <label>Project time</label>
                <input type="text" name="time" class="form-control" placeholder="Project time" value="{{ $quotation->time }}">
                {!! $errors->first('time', '<p class="help-block">:message</p>') !!}
            </div>

            <input type="submit" name="save" class="btn btn-md btn-success" value="Create">
            @if($quotation->quo === '')
                <input type="submit" name="draft" class="btn btn-md btn-warning" value="Draft">
            @endif
            <a class="btn btn-info" href="{{ url()->current() . '/item' }}">Edit Item</a>
        </form>
    </div>
</div>
@endsection
