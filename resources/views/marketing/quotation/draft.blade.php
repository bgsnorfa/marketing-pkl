@extends('layouts.marketing')


@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm text-muted" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>  
        </div>      
                                    <!-- /.main-search -->                             
    </div>
                               
    <div class="main-bar">
            <h3><i class="fa fa-file"></i>&nbsp;Quotation</h3>
        </div>
                            <!-- /.main-bar -->
        </header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
.label1 {;
  min-width: 60px !important;
  display: inline-block !important
}

</style>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Browse Quotations</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-file"></i> Quotation
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
    <div class="col-lg-12">
	
		 <table class="table table-condensed table-hover table-responsive ajaxdata" style="display:none">
		<thead>
		   <tr>
               
                <th>No</th>
                <th>QUO</th>
                <th>Consument</th>
				<th>Project</th> 
                <th>Price</th> 
                <th>Status</th> 
                <th>Action</th>
          
            </tr>
		</thead>
       
            
        <tbody id="success">
		</tbody>
     </table>
       <table class="table table-condensed table-hover table-responsive generaldata">
		<thead>
		   <tr>
                <th>No</th>
                <th>QUO</th>
                <th>Consument</th>
                <th>Project</th>
                <th>Price</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
		</thead>
			<?php $count = 1; ?>
            @foreach($quotation as $data)
         <tbody>
            <tr>
                <td>{{ $count }}</td>
                <td>{{ $data->quo }}</td>
                <td>{{ $data->name }}</td>
                <td>{{ $data->project }}</td>
                <td>{{number_format ($data->price) }}</td>
				@if($data->status ==='accepted')
			     <td ><span class="label label-success label-block label1"><b>Accepted</b></span></td>
				@elseif($data->status === 'rejected')
               <td><span class="label label-danger label-block label1"><b>Rejected</b></span></td>
            @else
             <td > <span class="label label-default label-block label1"><b>Waiting</b></span></td>
            @endif
                <td>
                    <a href="{{ url('/home/quotation/' . $data->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-info"></i></a>
                    @if($data->status === 'waiting')
						<a href="{{ url('/home/quotation/' . $data->id . '/edit') }}" class="btn btn-xs btn-default"><i class="fa fa-fw fa-pencil"></i></a>
                    @elseif($data->status === 'accepted')
                    <a href="{{ url('/Quotations/' . $data->file) }}" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-file-pdf-o"></i></a>
                    @endif
                </td>
            </tr>
			<?php $count++; ?> 
            @endforeach
		</tbody>
     </table> 
    </div>
</div>

        
    </div>
</div>

<script type="text/javascript">
    function search(){
        var search =$('#search').val();
        if(search){
            $(".generaldata").hide();
            $(".ajaxdata").show();
        }else{
            $(".generaldata").show();
            $(".ajaxdata").hide();
        }
        $.ajax({
            type: "POST",
            url: '{{URL::to("/home/quotation/search1")}}',
            data: {
                search: search,
                _token: $('#signup-token').val()    
            },
            datatype:'html',
            
            success: function(response){
                console.log(response);
                $("#success").html(response);
            }
        });
    }
</script>
@endsection
