@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Quotation</h3>
    </div>
                            <!-- /.main-bar -->
        </header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Edit Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('home/quotation') }}"><i class="fa fa-file"></i> Quotation</a>
            </li>
            <li>
                <a href="{{ url('home/quotation/' . $id . '/edit') }}">Edit</a>
            </li>
            <li>
                Item
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">

        <form method="POST" action="{{ url()->current() . '/add' }}">
            {{ csrf_field() }}
            <div class="form-group field_wrapper">
                <label>Project Items <a href="javascript:void(0);" class="add_button btn btn-info btn-xs" title="Add field">Add items</a></label>
                <br/>
                <div>
                    <input placeholder="Item" type="text" name="field_name[]" value=""/ required>
                    <input type="number" placeholder="Price" name="field_price[]" value=""/ required>
                </div>
            </div>

            <input type="submit" class="btn btn-success btn-sm" value="add">
        </form>

        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Item</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            @foreach($items as $indexKey=>$a)
            <tr>
                <td>{{ $indexKey+1 }}</td>
                <td>{{ $a->item }}</td>
                <td>{{ $a->harga }}</td>
                <td>
                    <a class="btn btn-info btn-xs" href="{{ url()->current() . '/' . $a->id }}"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-danger btn-xs" href="{{ url()->current() . '/' . $a->id .  '/delete' }}"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
