@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-file"></i>&nbsp;Quotation</h3>
    </div>
                            <!-- /.main-bar -->
        </header>

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotation <small>Edit Quotation</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('home/quotation') }}"><i class="fa fa-file"></i> Quotation</a>
            </li>
            <li>
                <a href="{{ url('home/quotation/' . $id . '/edit') }}">Edit</a>
            </li>
            <li>
                <a href="{{ url('home/quotation/' . $id . '/edit/item') }}">Item</a>
            </li>
            <li>
                {{ $item->item }}
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <form method="POST" action="{{ url()->current() }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label>Item name</label>
            <input type="text" name="item" value="{{ $item->item }}" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Item Price</label>
            <input type="number" name="price" value="{{ $item->harga }}" class="form-control" required>
        </div>
        <input type="submit" class="btn btn-success" value="Update">
    </form>
</div>
@endsection
