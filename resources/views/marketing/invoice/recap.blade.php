<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
	</head>

<body>
<style>
.table{
	border-collapse: collapse;
	position: relative;
	table-layout: fixed;
}
.table td{
	
	font-size: 8px;
}

.div {
    width: 300px;
    border: 2px solid orange;
    padding: 5px;
    margin: 5px;
}
.center{
	text-align:center;
}

.center{
	text-align:right;
}
.ttd{
	position: relative;
	margin-top: 10px;
	margin-left: 220px;
}
p{
	font-size: 8px;
}

.vl {
    border-right: 2px solid orange;
    height: 500px;
}
.vl1 {
    border-left: 2px solid orange;
    height: 500px;
}

.boo {
	margin-bottom: 30px;
	margin-top: 0px;
}
.boo1 {
	margin: 0;
}

.hr{
	height:2px;
	border-width:0;
	color:orange;
	background-color:orange;
	margin:0;
	margin-bottom: 5px;
}
</style>
<div class="">
<span style="margin:0; color:red;">
	Lokavor Studio
</span>

<h6 class="right" style="margin:0; margin-bottom: 10px;">
	Payment Receipt for {{$data->inv}}
</h6>
<hr class="hr">
<table class="table fixed">

<tr>
	<td colspan="3" width="60%">Detail :</td>
	<td colspan="2" width="40%">Consumer :</td>
</tr>
<tr>
	<td> Quo Number </td><td>{{$data->quo}}</td>
	<td></td>
	<td> Name </td><td>{{$cons->name}}</td>
</tr>

<tr>
	<td> Due Date </td><td>{{$data->duedate}}</td>
		<td></td>
	<td> Email </td><td>{{$cons->email}}</td>
</tr>

<tr>
	<td> Item</td><td>{{$data->item}}</td>
		<td></td>
	<td> Phone </td><td>{{$cons->phone}}</td>
</tr>

<tr>
	<td> Amount</td><td>{{number_format($data->amount)}}</td>
		<td></td>
	
</tr>



</table>
<hr class="hr" style="margin:0;">
        
	
	
	<div class="ttd">
	
	<p class="boo1">
	{{date('d F Y',strtotime(Carbon\Carbon::now()))}}
	</p>
	<p class="boo">Sincerely</p>
	
	<p style="margin:0;">
	{{Auth::user()->name}}
	</p>
	<p style="margin:0;">
	Marketing
	</p>
	</div>
	</body>
</div>
</html>