@extends('layouts.marketing')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm text-muted" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>  
        </div>      
        <!-- /.main-search -->                             
    </div>

     <div class="main-bar">
            <h3><i class="fa fa-credit-card"></i>&nbsp;Invoice</h3>
        </div>
                            <!-- /.main-bar -->
        </header>
    <!-- /.main-bar -->
</header>
<!-- /.head -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoice <small>Add Invoice</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-file"></i> Invoice
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">  
    <div class="col-lg-12">

       <table class="table table-condensed table-hover table-responsive ajaxdata" style="display:none">
        <thead>
         <tr>
        <th>No</th>
        <th>Quo</th>
        <th>Price(IDR)</th>
        <th>Bill(IDR)</th>
        <th>Paid(IDR)</th>
        <th>Unpaid(IDR)</th>
        <th>Action</th>
    </tr>
    </thead>


    <tbody id="success">
    </tbody>
</table>
<table class="table table-bordered table-hover table-condensed table-responsive generaldata">
     <thead>
         <tr>
        <th>No</th>
        <th>Quo</th>
        <th>Project</th>
        <th>Price(IDR)</th>
        <th>Bill(IDR)</th>
        <th>Paid(IDR)</th>
        <th>Unpaid(IDR)</th>
        <th>Action</th>
    </tr>
</thead>
    <?php $count = 1; ?>
    @foreach($quotation as $data)           
    <tbody>
    <tr>
        <td>{{ $count }}</td>
        <td>{{ $data->quo }}</td>
        <td>{{ $data->project }}</td>
        <td align="right">{{ number_format($data->price) }}</td>
        <td align="right">{{ number_format($data->bill) }}</td>
        <td align="right">{{ number_format($data->paid) }}</td>
        <td align="right">{{ number_format($data->price - $data->paid) }}</td>
        <td>

            <a href="{{ url('/home/invoice/' .$data->id. '/view') }}" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-info"></i></a>
            @if($data->price !== $data->bill)
            <a href="{{ url('/home/invoice/' .$data->id. '/payment') }}" class="btn btn-xs btn-success"><i class="fa fa-fw fa-money"></i></a>
            @endif
        </td>
    </tr>
    </tbody>
    <?php $count++; ?> 
    @endforeach
</table>
</div>
</div>

<script type="text/javascript">
    function search(){
        var search =$('#search').val();
        if(search){
            $(".generaldata").hide();
            $(".ajaxdata").show();
        }else{
            $(".generaldata").show();
            $(".ajaxdata").hide();
        }
        $.ajax({
            type: "POST",
            url: '{{URL::to("/home/invoice/search1")}}',
            data: {
                search: search,
                _token: $('#signup-token').val()    
            },
            datatype:'html',
            
            success: function(response){
                console.log(response);
                $("#success").html(response);
            }
        });
    }
</script>
@endsection
