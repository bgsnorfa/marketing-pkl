@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-credit-card"></i>&nbsp;Invoice</h3>
    </div>
                            <!-- /.main-bar -->
        </header>
                  
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoices <small>Edit Existing Invoice</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/invoice') }}"><i class="fa fa-user"></i> Invoice</a>
            </li>
            <li class="active">
                Edit
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
         <form method="post" action="{{ url()->current() }}">

            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('a') ? 'has-error' : '' }}">
                <label>Quotation</label>
                <input type="text" disabled name="a" class="form-control"  value="{{ $quotation->quo }}">
                {!! $errors->first('a', '<p class="help-block">:message</p>') !!}
            </div>
			
			<input type="hidden" name="quo" value="{{ $quotation->quo }}">

			<div class="form-group {{ $errors->has('unpaid') ? 'has-error' : '' }}">
                <label>Unpaid Amount</label>
                <input type="text" disabled name="unpaid" class="form-control"  value="{{ number_format($quotation->price - $total) }}">
                {!! $errors->first('unpaid', '<p class="help-block">:message</p>') !!}
            </div>

			<div class="form-group {{ $errors->has('item') ? 'has-error' : '' }}">
                <label>Item</label>
                <input type="text" name="item" class="form-control" value="{{ $data->item }}" placeholder="Item Name">
                {!! $errors->first('item', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('duedate') ? 'has-error' : '' }}">
                <label>Due Date</label>
                <input type="date" name="duedate" class="form-control" value="{{ $data->duedate }}" placeholder="Due Date">
                {!! $errors->first('duedate', '<p class="help-block">:message</p>') !!}
            </div>
			
            <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                <label>Payment Amount</label>
                <input type="number" name="amount"  min="1" max="{{ $quotation->price - $total }}" class="form-control" value="{{ $data->amount }}" placeholder="Payment Amount">
                {!! $errors->first('amount', '<p class="help-block">:message</p>') !!}
            </div>
            <input type="submit" name="save" value="Edit" class="btn btn-md btn-success">
        </form>
    </div>
</div>
@endsection
