@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<!-- Page Heading -->
<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Invoice</h3>
    </div>
                            <!-- /.main-bar -->
        </header>
                  

<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Invoice <small>{{ $quotation->quo }}</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/invoice') }}"><i class="fa fa-file"></i> Invoice</a>
            </li>
            <li class="active">
                {{ $quotation->quo }}
            </li>
        </ol>
    </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <a href="{{ url('/home/quotation') }}"><span class="glyphicon glyphicon-arrow-left"></span> Back to Quotation</a>
    </div>
</div>

<div class="row">
    <div clas="col-lg-12">
        <table class="table table-hover">
            <tr>
                <td>Project :</td>
                <td>{{ $quotation->project}}</td>
            </tr>

            <tr>
                <td>Project price(IDR):</td>
                <td align="right">{{ number_format($quotation->price) }}</td>
            </tr>

            <tr>
                <td>Amount paid(IDR):</td>
                <td align="right">{{ number_format($quotation->paid) }}</td>
            </tr>
			<tr>
                <td>Amount Unpaid(IDR):</td>
                <td align="right">{{number_format($quotation->price-$quotation->paid) }}</td>
            </tr>
			<tr>
                <td colspan='2'><h3>Detail Invoice</h3></td>
                
            </tr>

			<tr>
                <th>No</th>
                <th>Invoice No</th>
                <th>Date Paid</th>
                <th>Amount(IDR)</th>   
			</tr>
			<?php $count = 1; ?>
		@foreach($invoice as $invoice)
			<tr>
				<td>{{$count}}</td>
				<td>{{$invoice->inv}}</td>
				<td>{{$invoice->created_at}}</td>
				<td align="right">{{number_format($invoice->amount)}}</td>
			</tr>
			<?php $count =$count+ 1; ?>
			@endforeach
		</table>
    </div>
</div>
@endsection
