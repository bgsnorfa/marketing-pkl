@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->

<header class="head">


    <div class="main-bar">
        <h3><i class="fa fa-wrench"></i>&nbsp;After Sale</h3>
    </div>
                            <!-- /.main-bar -->
        </header>


<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            After Sales <small>Edit Existing After Sale</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('home/aftersale') }}"><i class="fa fa-bookmark"></i> After Sale</a>
            </li>
            <li class="active">
                Edit
            </li>
        </ol>
    </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">

      {{ csrf_field() }}

      <div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
    <label for="input">Start Date</label>
      <input type="date" name="startdate" class="form-control" placeholder="Input contract name" value="{{ $aftersale->startdate }}" disabled>
      <input type="hidden" name="startdate" class="form-control" placeholder="Input contract name" value="{{ $aftersale->startdate }}">
      {!! $errors->first('startdate', '<p class="help-block">:message</p>') !!}
  </div>

  <div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
    <label for="input">End Date</label>
      <input type="date" name="enddate" class="form-control" placeholder="Input contract name" value="{{ $aftersale->enddate }}">
      {!! $errors->first('enddate', '<p class="help-block">:message</p>') !!}
  </div>


      <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
        <label>Upload new After Sale file (ignore if don't want to change after sale file)</label>
        <br/>
        <a class="btn btn-xs btn-primary" href="{{ url( 'aftersales/' . $aftersale->file ) }}"><i class="fa fa-fw fa-folder-open"></i> View current Contract file</a>
        <input type="file" name="file" class="form-control">
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
      </div>

      <input type="submit" name="update" class="btn btn-success btn-md" value="Update">

    </form>
  </div>
</div>
@endsection
