@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->

<header class="head">
    
                               
    <div class="main-bar">
        <h3><i class="fa fa-wrench"></i>&nbsp;After Sale</h3>
    </div>
                            <!-- /.main-bar -->
        </header>


<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            After Sales <small>Add new After Sale</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{ url('/home/aftersale') }}"><i class="fa fa-bookmark"></i> After Sale</a>
            </li>
            <li class="active">
              Add
            </li>
        </ol>
    </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">

      {{ csrf_field() }}

      <div class="form-group {{ $errors->has('quo') ? 'has-error' : '' }}">
        <label>Select Quotation</label>
        <select class="form-control" name="quo" required="required">
          <option value="">--- Select Quotation ---</option>
          @foreach($quotation as $data)
          <option value={{ $data->quo }}>{{ $data->quo }} - {{ $data->project }}</option>
          @endforeach
        </select>
        {!! $errors->first('quo', '<p class="help-block">:message</p>') !!}
      </div>

     <div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
		<label for="input">Start Date</label>
			<input type="date" name="startdate" class="form-control" placeholder="Input contract name" value="{{ old('startdate') }}">
			{!! $errors->first('startdate', '<p class="help-block">:message</p>') !!}
	</div>
			
	<div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
		<label for="input">End Date</label>
			<input type="date" name="enddate" class="form-control" placeholder="Input contract name" value="{{ old('enddate') }}">
			{!! $errors->first('enddate', '<p class="help-block">:message</p>') !!}
	</div>
				
	<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
		<label for="input">File to upload</label>
			<input type="file" name="file" class="form-control" placeholder="input file">
			{!! $errors->first('file', '<p class="help-block">:message</p>') !!}
	</div>

      <input type="submit" name="save" class="btn btn-md btn-success" value="Save">
    </form>
  </div>
</div>
@endsection
