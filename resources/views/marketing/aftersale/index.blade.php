@extends('layouts.marketing')

@section('heading')
<!-- Page Heading -->
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm text-muted" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>  
        </div>      
        <!-- /.main-search -->                             
    </div>

    <div class="main-bar">
            <h3><i class="fa fa-wrench"></i>&nbsp;After Sale</h3>
        </div>
    <!-- /.main-bar -->
</header>
<!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">

    <div class="col-lg-12">
        <h1 class="page-header">
            After Sales <small>Browse After Sales</small>
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <li class="active">
                <i class="fa fa-bookmark"></i> After Sale
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}"> 
  <div class="col-lg-12">
    <table class="table table-condensed table-hover table-responsive ajaxdata" style="display:none">
        <thead>
         <tr>

            <th>No</th>
            <th>QUO</th>
            <th>Star Date</th>
            <th>End Date</th> 
            <th>Action</th>

        </tr>
    </thead>


    <tbody id="success">
    </tbody>
</table>
    <table class="table table-bordered table-responsive table-hover table-condensed generaldata">
      <thead>
      <tr>
        <th>No</th>
        <th>QUO</th>
        <th>Project</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Action</th>
      </tr>
    </thead>
    <?php $count = 1; ?>
      @foreach($aftersales as $data)
     
      <tbody>
      <tr>
        <td>{{ $count }}</td>
        <td>{{ $data->quo }}</td>
        <td>{{ $data->project}}</td>
        <td>{{ $data->startdate }}</td>
        <td>{{ $data->enddate }}</td>
        <td>
          <a target="_blank" href="{{ url('/aftersales/' . $data->file) }}" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-file-pdf-o"></i></a>
          <a href="{{ url('/home/aftersale/' . $data->id . '/edit') }}" class="btn btn-default btn-xs"><i class="fa fa-fw fa-pencil"></i></a>
        </td>
      </tr>
      </tbody>
       <?php $count = $count+1; ?>
      @endforeach
    </table>
  </div>
</div>
<script type="text/javascript">
    function search(){
        var search =$('#search').val();
        if(search){
            $(".generaldata").hide();
            $(".ajaxdata").show();
        }else{
            $(".generaldata").show();
            $(".ajaxdata").hide();
        }
        $.ajax({
            type: "POST",
            url: '{{URL::to("/home/aftersale/search")}}',
            data: {
                search: search,
                _token: $('#signup-token').val()    
            },
            datatype:'html',
            
            success: function(response){
                console.log(response);
                $("#success").html(response);
            }
        });
    }
</script>
@endsection
