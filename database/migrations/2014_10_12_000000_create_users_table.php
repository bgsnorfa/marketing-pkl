<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('role', ['marketing', 'superadmin', 'finance'])->default('marketing');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            [
                'name' => 'admin',
                'role' => 'superadmin',
                'email' => 'admin@mail.com',
                'password' => bcrypt('katakunci')
            ],
            [
                'name' => 'Faiz Gilang',
                'role' => 'marketing',
                'email' => 'brave.holy.knight@gmail.com',
                'password' => bcrypt('katakunci')
            ],
            [
                'name' => 'Arifuddin',
                'role' => 'marketing',
                'email' => 'maarifuddin17@gmail.com',
                'password' => bcrypt('katakunci')
            ],
            [
                'name' => 'Finance',
                'role' => 'finance',
                'email' => 'finance@mail.com',
                'password' => bcrypt('katakunci')
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
