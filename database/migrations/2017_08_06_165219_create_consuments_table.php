<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consuments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->text('address');
            $table->enum('status', ['prospecting', 'contacted'])->default('prospecting');
            $table->integer('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        DB::table('consuments')->insert([
            [
                'name' => 'PT AMB',
                'email' => 'admin@ptamb.id',
                'phone' => '081326443888',
                'address' => 'Kampus Undip Tembalang',
                'user' => '2'
            ],
            [
                'name' => 'PT Undip Maju',
                'email' => 'contact@undipmaju.com',
                'phone' => '024111456',
                'address' => 'Widya Puraya Undip',
                'user' => '2'
            ],
			[
				'name' => 'Muhammad Arif',
				'email' => 'maarifuddin17@gmail.com',
				'phone' => '08735311985',
				'address' => 'Jalan Mawar 84 Ungaran Timur',
				'user' => '3'
			],
            [
                'name' => 'Pak Herry FPIK',
                'email' => 'herry@undip.ac.id',
                'phone' => '081390989819',
                'address' => 'gedung FPIK Undip',
                'user' => '3'
            ],
            [
                'name' => 'Ahmad Latif',
                'email' => 'aalatif@gmail.com',
                'phone' => '087735391924',
                'address' => 'Sirojudin Gang Bharata no.23A Tembalang',
                'user' => '3'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consuments');
    }
}
