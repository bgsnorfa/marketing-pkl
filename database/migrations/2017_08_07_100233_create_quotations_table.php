<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quo')->default('');
            $table->integer('consument')->unsigned();
            $table->foreign('consument')->references('id')->on('consuments')->onDelete('cascade');
            $table->string('project');
            $table->integer('price');
            $table->integer('paid')->default('0');
            $table->integer('bill')->default('0');
			 $table->text('file')->nullable();
            // $table->text('term')->nullable();
            $table->string('time')->nullable();
            // $table->text('spec')->nullable();
            $table->enum('status', ['accepted', 'waiting', 'rejected'])->default('waiting');
            $table->enum('oncontract', ['yes', 'no'])->default('no');
            $table->enum('onaftersale', ['yes', 'no'])->default('no');
            $table->integer('user')->unsigned();
            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotations');
    }
}
