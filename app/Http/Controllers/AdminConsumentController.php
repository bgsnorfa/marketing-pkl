<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Consument;
use App\User;
use DB;
use Excel;
class AdminConsumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadmin');
    }

    public function index(Request $request)
    {
        if ($request->has('user')) {
            $consuments = DB::table('consuments')
                ->join('users', 'consuments.user', '=', 'users.id')
                ->select('consuments.*', 'users.name as user_name')
                ->where('consuments.user', '=', $request->user)
                ->paginate(10);
        } else {
            $consuments = DB::table('consuments')
                ->join('users', 'consuments.user', '=', 'users.id')
                ->select('consuments.*', 'users.name as user_name')
                ->paginate(10);
        }

        $marketing = User::where('role', '=', 'marketing')
            ->get();

        return view('superadmin.consument.index')
            ->with('consuments', $consuments)
            ->with('marketing', $marketing);
    }

    public function view($id)
    {
        $consument = DB::table('consuments')
            ->join('users', 'consuments.user', '=', 'users.id')
            ->select('consuments.*', 'users.name as user_name')
            ->where('consuments.id', '=', $id)
            ->first();

        return view('superadmin.consument.view')
            ->with('consument', $consument);
    }

    public function export()
    {
        $get = Consument::get();
        $year = array();
        foreach($get as $a) {
            if (! in_array($a->created_at->format('Y'), $year)) {
                array_push($year, $a->created_at->format('Y'));
            }
        }

        return view('superadmin.consument.export')
            ->with('year', $year);
    }

    public function toExcel(Request $request) {
         $GLOBALS['data'] = DB::table('consuments')
            ->join('users', 'consuments.user', '=', 'users.id')
            ->select('consuments.*', 'users.name as marketing_name')
            ->whereYear('consuments.created_at', '=', $request->year)
            ->whereMonth('consuments.created_at', '=', $request->month)
            ->get();

        Excel::create('Data_Consument_' . $request->year . '-' . $request->month, function ($excel) {
            $excel->sheet('consument', function ($sheet) {
                $sheet->loadView('superadmin.excel.consument')
                    ->with('data', $GLOBALS['data']);
            });
        })->download('xlsx');

        return redirect('superadmin/consument/export');
    }
}
