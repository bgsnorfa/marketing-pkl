<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consument;
use Auth;

class ConsumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('marketing');
    }
    public function index()
    {
        $data = Consument::where('user', '=', Auth::user()->id)->get();
        return view('marketing.consument.index')
        ->with('title', 'Consument')
        ->with('menu', 'consument')
        ->with('consuments', $data);
    }
	
	public function search(Request $request){	
		if($request->search)
		{
        $data = Consument::where('user', '=', Auth::user()->id)
		->where('name','like','%'.$request->search.'%')
		->get();
		if($data){
			$count=1;
			foreach($data as $key => $data){
				if($data->status == 'prospecting')
				{				
                        $contact='<a href="/home/consument/' .$data->id. '/contact" class="btn btn-xs btn-metis-6"><i class="fa fa-fw fa-phone"></i></a>'; 
						$a= '<span class="label label-default label-block label1"><b>Prospecting</b></span>';  
				}
				if($data->status == 'contacted')
				{				
                        $contact='<a href="/home/quotation/add/consument/' . $data->id.'" class="btn btn-xs btn-success"><i class="fa fa-fw fa-file"></i></a>'; 
						$a= '<span class="label label-success label-block label1"><b>Contacted</b></span>';  
				}
 
				echo'<tr>.
				<td>'.$count.'</td>.
				<td>'.$data->name.'</td>.
				<td>'.$data->email.'</td>.
				<td>'.$data->phone.'</td>.
				<td>'.$data->address.'</td>.
				<td>'.$a.'</td>.
				<td>'. '<a href="/home/consument/' . $data->id . '/edit" class="btn btn-xs btn-default"><i class="fa fa-fw fa-pencil"></i></a>'.' '.$contact.'</td></tr>';
			$count=$count+1;
			}
		}
		}
	}

    public function contact($id)
    {
        $data = Consument::find($id);
        $data->status = 'contacted';
        $data->save();
        return redirect('home/consument');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marketing.consument.add')
        ->with('title', 'Add new Consument')
        ->with('menu', 'consument');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|unique:consuments',
            'phone' => 'required|numeric',
            'address' => 'required'
        ]);

        $consument = new Consument;
        $consument->name = $request->name;
        $consument->email = $request->email;
        $consument->phone = $request->phone;
        $consument->address = $request->address;
        $consument->user = Auth::user()->id;

        $consument->save();

        return redirect('/home/consument');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Consument::find($id);
        return view('marketing.consument.edit')
        ->with('title', 'Edit Consument')
        ->with('menu', 'consument')
        ->with('consument', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $consument = Consument::find($id);

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:consuments,email,' . $consument->id,
            'phone' => 'required|numeric',
            'address' => 'required'
        ]);

        $consument->name = $request->name;
        $consument->email = $request->email;
        $consument->phone = $request->phone;
        $consument->address = $request->address;

        $consument->save();
        return redirect('home/consument');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
