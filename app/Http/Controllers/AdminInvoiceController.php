<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use App\User;
use App\Consument;
use App\Invoices;
use DB;
use Excel;

class AdminInvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadmin');
    }

    public function index(Request $request)
    {
        if ($request->has('user')) {
            $quotations = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.user', '=', $request->user)
                ->where('quotations.quo', '!=', '')
                ->where('quotations.status', '=', 'accepted')
                ->orderBy('quotations.created_at', 'desc')
                ->paginate(10);
        } else {
            $quotations = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.quo', '!=', '')
                ->where('quotations.status', '=', 'accepted')
                ->orderBy('quotations.created_at', 'desc')
                ->paginate(10);
        }

        $marketing = User::where('role', '=', 'marketing')
            ->get();

        return view('superadmin.invoice.index')
            ->with('quotations', $quotations)
            ->with('marketing', $marketing);
    }


    public function view($id)
    {

		$quotation = Quotation::find($id);
		$inv= Invoices::where('quo','=',$quotation->quo)->get();
        $consument = Consument::find($quotation->consument);

        return view('superadmin.invoice.view')
            ->with('invoice', $inv)
            ->with('quotation', $quotation)
            ->with('consument', $consument);
    }

    public function export()
    {
        $year = array();
        $get = DB::table('invoices')
            ->select('invoices.*')
            ->get();

        foreach ($get as $a) {
            if (! in_array(date('Y', strtotime($a->created_at)), $year)) {
                array_push($year, date('Y', strtotime($a->created_at)));
            }
        }

        return view('superadmin.invoice.export')
            ->with('year', $year);
    }

    public function toExcel(Request $request)
    {
        $GLOBALS['inv'] = DB::table('invoices')
            ->select('invoices.*')
            ->whereYear('created_at', '=', $request->inv_year)
            ->whereMonth('created_at', '=', $request->inv_month)
            ->where('status', '=', 'paid')
            ->get();

        // dd($GLOBALS['inv']);

        Excel::create('Data_Invoice_' . $request->inv_year . '-' . $request->inv_month, function ($excel) {
            $excel->sheet('Invoices', function ($sheet) {
                $sheet->loadView('superadmin.excel.invoice')
                    ->with('inv', $GLOBALS['inv']);
            });
        })->download('xlsx');
        return redirect('superadmin/invoice/export');
    }

    public function exportSingle($id)
    {
        $GLOBALS['quo'] = Quotation::find($id);
        $GLOBALS['consumer'] = Consument::find($GLOBALS['quo']->consument);
        $GLOBALS['inv'] = Invoices::where('quo', '=', $GLOBALS['quo']->quo)
            ->get();

        Excel::create('Invoices-' . $GLOBALS['quo']->quo, function ($excel) {
            $excel->sheet('Invoices', function ($sheet) {
                $sheet->loadView('superadmin.excel.single_invoice')
                    ->with('quo', $GLOBALS['quo'])
                    ->with('inv', $GLOBALS['inv'])
                    ->with('consumer', $GLOBALS['consumer']);
            });
        })->download('xlsx');
        return redirect('superadmin/invoice');
    }
}
