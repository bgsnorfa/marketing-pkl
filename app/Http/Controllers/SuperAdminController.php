<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quotation;
use App\Consument;
use App\Contract;
use App\Invoices;
use App\AfteSale;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;


class SuperAdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadmin');
    }

        public function index(request $request)
    {
            $marketing= User::where('role','=','marketing')->get();
        if ($request->has('user')) {

            $cons = Consument::where('user','=',$request->user)->count();

        $x = Consument::where('user','=',$request->user)->get();
        $cons1=0;
        foreach($x as $key => $x){
        $a = Quotation::where('consument','=',$x->id)->count();

            if($a != 0)
            {
                $cons1=$cons1+1;
            }
        }

        $y = Consument::where('user','=',$request->user)->get();
        $cons2=0;
        foreach($y as $key => $y){
        $b = Quotation::where('consument','=',$y->id)->where('oncontract','=','yes')->count();

            if($b != 0)
            {
                $cons2=$cons2+1;
            }
        }
        $quo = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.user', '=', $request->user)
            ->get();

        $quoall = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.user', '=', $request->user)
            ->count();

        $quowait = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'waiting')
            ->where('quotations.user', '=', $request->user)
            ->count();

        $quoacc = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'accepted')
            ->where('quotations.user', '=', $request->user)
            ->count();

        $quorejec = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'rejected')
            ->where('quotations.user', '=', $request->user)
            ->count();

        $cont=Contract::count();
    //  $cont1=DB::table('contracts')
    //  ->join('quotations','quotation.qui','=','contracts.quo')
    //  ->where('quotations.user', '=', Auth::user()->id)
    //  -count();

        $inv=Quotation::where('quotations.quo', '!=', '')
        ->where('quotations.user', '=',$request->user)
        ->where('quotations.status', '=', 'accepted')->get();


      $as = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', $request->user)
        ->count();

        $expDate = Carbon::now()->addDays(30);
        $variable2 =  DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', $request->user)->whereDate('enddate', '<',$expDate)->get()->count();

        $aftersale2 = $as-$variable2;

        } else {

		$cons = Consument::count();

		$x = Consument::get();
        $cons1=0;
        foreach($x as $key => $x){
        $a = Quotation::where('consument','=',$x->id)->count();

            if($a != 0)
            {
                $cons1=$cons1+1;
            }
        }

        $y = Consument::get();
        $cons2=0;
        foreach($y as $key => $y){
        $b = Quotation::where('consument','=',$y->id)->where('oncontract','=','yes')->count();

            if($b != 0)
            {
                $cons2=$cons2+1;
            }
        }
		$quo = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->get();

		$quoall = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->count();

		$quowait = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'waiting')
            ->count();

		$quoacc = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'accepted')
            ->count();

		$quorejec = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'rejected')
            ->count();

		$cont=Contract::count();
	//	$cont1=DB::table('contracts')
	//	->join('quotations','quotation.qui','=','contracts.quo')
	//	->where('quotations.user', '=', Auth::user()->id)
	//	-count();

		$inv=Quotation::where('quotations.quo', '!=', '')
		->where('quotations.status', '=', 'accepted')->get();


      $as = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->count();

        $expDate = Carbon::now()->addDays(30);
        $variable2 =  DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')->whereDate('enddate', '<',$expDate)->get()->count();

        $aftersale2 = $as-$variable2;
}
        return view('superadmin.index')
        ->with('title', 'Dashboard')
        ->with('consument', $cons)
        ->with('contract', $cont)
        ->with('consument1', $cons1)
        ->with('consument2', $cons2)
        ->with('quotation', $quo)
        ->with('quotation1', $quoall)
        ->with('quotation2', $quowait)
        ->with('quotation3', $quoacc)
        ->with('quotation4', $quorejec)
        ->with('invoice', $inv)
        ->with('aftersale', $as)
        ->with('aftersale1', $variable2)
        ->with('aftersale1', $variable2)
        ->with('aftersale2', $aftersale2)
        ->with('marketing', $marketing);
    }

    public function userView()
    {
        $users = User::where('role', '!=', 'superadmin')
            ->get();

        return view('superadmin.userview')
            ->with('users', $users);
    }

    public function userViewEdit($id)
    {
        $user = User::find($id);
        if ($user->role === 'superadmin') {
            return redirect('superadmin/user/view');
        }

        return view('superadmin.useredit')
            ->with('user', $user);
    }

    public function userViewUpdate($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        return redirect('superadmin/user/view');
    }

    public function userViewDelete($id)
    {
        $user = User::find($id);
        if ($user->role !== 'superadmin') {
            $user->delete();
        }
        return redirect('superadmin/user/view');
    }

    public function user()
    {
        return view('superadmin.user');
    }

    public function userStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'exists:users,role'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $user->role = $request->role;
        $user->save();

        $users = User::where('role', '!=', 'superadmin')
            ->get();

        return view('superadmin.userview')
            ->with('flash', 'a')
            ->with('users', $users);
    }

}
