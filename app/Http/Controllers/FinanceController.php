<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('finance');
    }

    public function index()
    {
        return view('finance.index')
            ->with('menu', 'index');
    }
}
