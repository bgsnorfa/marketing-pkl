<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\AfterSale;
use App\Quotation;
use DB;
use Excel;

class AdminAfterSaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadmin');
    }

    public function index(Request $request)
    {
        if ($request->has('user')) {
            $aftersales = DB::table('after_sales')
                ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
                ->select('after_sales.*')
                ->where('quotations.user', '=', $request->user)
                ->orderBy('after_sales.created_at', 'desc')
                ->paginate(10);
        } else {
            $aftersales = DB::table('after_sales')
                ->orderBy('after_sales.created_at', 'desc')
                ->paginate(10);
        }

        $marketing = User::where('role', '=', 'marketing')
            ->get();

        return view('superadmin.aftersale.index')
            ->with('aftersales', $aftersales)
            ->with('marketing', $marketing);
    }

    public function view($id)
    {
        $aftersale = DB::table('after_sales')
            ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
            ->join('consuments', 'consuments.id', '=', 'quotations.consument')
            ->select('after_sales.*', 'quotations.project as q_project', 'consuments.name as c_name')
            ->first();

        return view('superadmin.aftersale.view')
            ->with('aftersale', $aftersale);
    }

    public function export()
    {
        $marketing = DB::table('users')
            ->where('users.role', '=', 'marketing')
            ->get();

        return view('superadmin.aftersale.export')
            ->with('marketing', $marketing);
    }

    public function toExcel(Request $request)
    {
        if ($request->as_marketing !== 'all') {
            $takeName = DB::table('users')
                ->select('users.name')
                ->where('users.id', '=', $request->as_marketing)
                ->first();
            $name = $takeName->name;
        } else {
            $name = 'All';
        }

        if ($request->as_marketing === 'all') {
            $GLOBALS['data'] = DB::table('after_sales')
                ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
                ->join('consuments', 'consuments.id', '=', 'quotations.consument')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('after_sales.*', 'quotations.quo as q_quo', 'quotations.project as q_project', 'consuments.name as c_name', 'users.name as u_name')
                ->whereYear('after_sales.created_at', '=', $request->as_year)
                ->whereMonth('after_sales.created_at', '=', $request->as_month)
                ->get();
        } else {
            $GLOBALS['data'] = DB::table('after_sales')
                ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
                ->join('consuments', 'consuments.id', '=', 'quotations.consument')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('after_sales.*', 'quotations.quo as q_quo', 'quotations.project as q_project', 'consuments.name as c_name', 'users.name as u_name')
                ->whereYear('after_sales.created_at', '=', $request->as_year)
                ->whereMonth('after_sales.created_at', '=', $request->as_month)
                ->where('quotations.user', '=', $request->as_marketing)
                ->get();
        }

        Excel::create('Data_AferSales' . $name . '_' . $request->as_year . '-' . $request->as_month, function ($excel) {
            $excel->sheet('AfterSales', function ($sheet) {
                $sheet->loadView('superadmin.excel.aftersale')
                    ->with('data', $GLOBALS['data']);
            });
        })->download('xlsx');
    }
}
