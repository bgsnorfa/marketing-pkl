<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;
use App\User;
use App\Quotation;
use DB;
use Excel;

class AdminContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadmin');
    }

    public function index(Request $request)
    {
        if ($request->has('user')) {
            $contracts = DB::table('contracts')
                ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('contracts.*', 'users.name as u_name')
                ->where('quotations.user', '=', $request->user)
                ->orderBy('contracts.created_at', 'desc')
                ->paginate(10);
        } else {
            $contracts = DB::table('contracts')
                ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('contracts.*', 'users.name as u_name')
                ->orderBy('contracts.created_at', 'desc')
                ->paginate(10);
        }

        $marketing = User::where('role', '=', 'marketing')
            ->get();

        return view('superadmin.contract.index')
            ->with('contracts', $contracts)
            ->with('marketing', $marketing);
    }

    public function view($id)
    {
        $contract = DB::table('contracts')
            ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
            ->join('users', 'quotations.user', '=', 'users.id')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('contracts.*', 'quotations.project as q_project', 'users.name as u_name', 'consuments.name as c_name')
            ->where('contracts.id', '=', $id)
            ->first();

        return view('superadmin.contract.view')
            ->with('contract', $contract);
    }

    public function export()
    {
        $marketing = DB::table('users')
            ->select('users.*')
            ->where('users.role', '=', 'marketing')
            ->get();

        return view('superadmin.contract.export')
            ->with('marketing', $marketing);
    }

    public function toExcel(Request $request)
    {
        if ($request->marketing !== 'all') {
            $takeName = DB::table('users')
                ->select('users.name')
                ->where('users.id', '=', $request->marketing)
                ->first();
            $name = $takeName->name;
        } else {
            $name = 'All';
        }

        if ($request->marketing === 'all') {
            $GLOBALS['data'] = DB::table('contracts')
                ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->join('consuments', 'consuments.id', '=', 'quotations.consument')
                ->select('contracts.*', 'quotations.project as q_project', 'users.name as u_name', 'consuments.name as c_name')
                ->get();
        } else {
            $GLOBALS['data'] = DB::table('contracts')
                ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->join('consuments', 'consuments.id', '=', 'quotations.consument')
                ->select('contracts.*', 'quotations.project as q_project', 'users.name as u_name', 'consuments.name as c_name')
                ->where('quotations.user', '=', $request->marketing)
                ->get();
        }

        Excel::create('Data_Contracts_' . $name . '_' . $request->contract_year . '-' . $request->contract_month, function ($excel) {
            $excel->sheet('Contracts', function ($sheet) {
                $sheet->loadView('superadmin.excel.contract')
                    ->with('data', $GLOBALS['data']);
            });
        })->download('xlsx');
    }
}
