<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quotation;
use App\QuotationDetail;
use App\Consument;
use Auth;
use Illuminate\Support\Facades\Input;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('marketing');
    }

    public function index()
    {
        $data = DB::table('quotations')
        ->join('consuments', 'quotations.consument', '=', 'consuments.id')
        ->select('quotations.*', 'consuments.name')
        ->where('quotations.quo', '!=', '')
        ->where('quotations.user', '=', Auth::user()->id)
        ->get();

        return view('marketing.quotation.index')
        ->with('title', 'Quotations')
        ->with('menu', 'quotation')
        ->with('quotation', $data);
    }

    public function search(Request $request){
      if($request->search)
      {

        $data = DB::table('quotations')
        ->join('consuments', 'quotations.consument', '=', 'consuments.id')
        ->select('quotations.*', 'consuments.name')
        ->where('quotations.quo', '!=', '')
        ->where('quotations.user', '=', Auth::user()->id)
        ->where('quotations.quo','like','%'.$request->search.'%')
        ->get();

        if($data){
         $count=1;
         foreach($data as $key => $data){
            if($data->status == 'waiting')
            {
                $contact='
				<a href="/home/quotation/' .$data->id. '/edit" class="btn btn-xs btn-default"><i class="fa fa-fw fa-pencil"></i></a>.
                <a href="/home/quotation/' .$data->id. '/accept" class="btn btn-xs btn-success"><i class="fa fa-fw fa-check"></i></a>.
                <a href="/home/quotation/' .$data->id. '/reject" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-times"></i></a>';
				$a= '<span class="label label-default label-block label1"><b>Waiting</b></span>';  
            }
            elseif($data->status == 'accepted')
            {
                $contact='
                <a href="/Quotations/' .$data->file.'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-file-pdf-o"></i></a>';
				$a= '<span class="label label-success label-block label1"><b>Accepted</b></span>';  
            }
            else
            {
                $contact='';
				$a= '<span class="label label-danger label-block label1"><b>Rejected</b></span>';  
            }
        
            echo'<tr>';
            echo '<td>'.$count.'</td>.
            <td>'.$data->quo.'</td>.
            <td>'.$data->name.'</td>.
            <td>'.$data->project.'</td>.
            <td>'.number_format($data->price).'</td>.
            <td>'.$a.'</td>.
            <td>'. '<a href="/home/quotation/' . $data->id .'" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-info"></i></a>'.' '.$contact.'</td></tr>';
            $count=$count+1;
        }
    }
}
}
public function search1(Request $request){
      if($request->search)
      {

        $data = DB::table('quotations')
        ->join('consuments', 'quotations.consument', '=', 'consuments.id')
        ->select('quotations.*', 'consuments.name')
        ->where('quotations.quo', '=', '')
        ->where('quotations.user', '=', Auth::user()->id)
        ->where('consuments.name','like','%'.$request->search.'%')
        ->get();

        if($data){
         $count=1;
         foreach($data as $key => $data){
            if($data->status == 'waiting')
            {
                $contact='
                <a href="/home/quotation/' .$data->id. '/edit" class="btn btn-xs btn-default"><i class="fa fa-fw fa-pencil"></i></a>';
            }
            else
            {
                $contact='';
            }

            echo'<tr>.
            <td>'.$count.'</td>.
            <td>'.$data->quo.'</td>.
            <td>'.$data->name.'</td>.
            <td>'.$data->project.'</td>.
            <td>'.number_format($data->price).'</td>.
            <td>'.'<span class="label label-default label-block label1"><b>Waiting</b>'.'</td>.
            <td>'. '<a href="/home/quotation/' . $data->id . '" class="btn btn-xs btn-primary"><i class="fa fa-fw fa-info"></i></a>'.' '.$contact.'</td></tr>';
            $count=$count+1;
        }
    }
}
}

public function draft()
{
    $data = DB::table('quotations')
    ->join('consuments', 'quotations.consument', '=', 'consuments.id')
    ->select('quotations.*', 'consuments.name')
    ->where('quotations.quo', '=', '')
    ->where('quotations.user', '=', Auth::user()->id)
    ->paginate(10);

    return view('marketing.quotation.draft')
    ->with('title', 'Quotations Draft')
    ->with('menu', 'quotation')
    ->with('quotation', $data);
}

public function accept($id){

     $quotation = Quotation::find($id);
     return view('marketing.quotation.accept')
	 ->with('title', 'Quotations Draft')
	 ->with('menu', 'quotation')
	 ->with('quotation',$quotation);
}
public function storeaccept(Request $request, $id)
{
    $quotation = Quotation::find($id);
    $this->validate($request, [
            'file' => 'required|file|mimetypes:application/pdf',
        ]);

    if ($quotation->user != Auth::user()->id) {
        return redirect('/home/quotation');
    }

    if ($quotation->user === Auth::user()->id && $quotation->status === 'waiting' && $quotation->quo != '') {

        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'Quotation-' . $quotation->quo . '.' . $ext;
        $request->file('file')->move('quotations/', $filename);

        $quotation->file = $filename;
        $quotation->status = 'accepted';
        $quotation->save();
    }

    return redirect('home/quotation');
}

public function reject($id)
{
    $quotation = Quotation::find($id);
    if ($quotation->user != Auth::user()->id) {
        return redirect('/home/quotation');
    }

    if ($quotation->user === Auth::user()->id && $quotation->status === 'waiting' && $quotation->quo != '') {
        $quotation->status = 'rejected';
        $quotation->save();
    }

    return redirect('home/quotation');
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marketing.quotation.add')
        ->with('title', 'Add new Quotation')
        ->with('menu', 'quotation');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->save)) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:consuments',
                'phone' => 'required|numeric',
                'address' => 'required',
                'project' => 'required',
                'time' => 'required',
            ]);

            //add consument data to database
            $consument = new Consument;
            $consument->name = $request->name;
            $consument->email = $request->email;
            $consument->phone = $request->phone;
            $consument->address = $request->address;
            $consument->user = Auth::user()->id;
            $consument->status = 'contacted';

            $consument->save();

            $total = 0;
            foreach($request->field_price as $add) {
                $total = $total + $add;
            }

            //add quotation data to database
            $quotation = new Quotation;
            $quotation->project = $request->project;
            $quotation->time = $request->time;
            $quotation->price = $total;
            $quotation->consument = $consument->id;
            $quotation->user = Auth::user()->id;
            $quotation->save();

            //create quotation number
            $urutan = Quotation::
            whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->where('quo', '!=', '')
            ->count();
            $urutan = $urutan + 1;
            if ($urutan < 10) {
                $urutan  = '00'. $urutan;
            } else if ($urutan < 100 && $urutan > 9) {
                $urutan = '0' .$urutan;
            }

            //update last inserted quotation
            $new = Quotation::find($quotation->id);
            $new->quo = 'QUO' . date('m') . date('y') . $urutan;
            $new->save();

            $current_quo = $new->id;

            if ($total > 0) {
                foreach ($request->field_name as $key=>$iter) {
                    $detail = new QuotationDetail();
                    $detail->id_quo = $current_quo;
                    $detail->item = $iter;
                    $detail->harga = $request->field_price[$key];
                    $detail->save();
                }
            }

            return redirect('home/quotation');
        }

		if (isset($request->draft)) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|unique:consuments',
                'phone' => 'required|numeric',
                'address' => 'required',
                'project' => 'required'
            ]);

            //add consument data to database
            $consument = new Consument;
            $consument->name = $request->name;
            $consument->email = $request->email;
            $consument->phone = $request->phone;
            $consument->address = $request->address;
            $consument->user = Auth::user()->id;
            $consument->status = 'contacted';

            $consument->save();

            $total = 0;
            foreach($request->field_price as $add) {
                $total = $total + $add;
            }

            $quotation = new Quotation;
            $quotation->consument = $consument->id;
            $quotation->user = Auth::user()->id;
            $quotation->project = $request->project;
            $quotation->price = $total;

            if ($request->time != '') {
                $quotation->time = $request->time;
            }

            $quotation->save();

            $current_quo = $quotation->id;

            if ($total > 0) {
                foreach ($request->field_name as $key=>$iter) {
                    $detail = new QuotationDetail();
                    $detail->id_quo = $current_quo;
                    $detail->item = $iter;
                    $detail->harga = $request->field_price[$key];
                    $detail->save();
                }
            }

            return redirect('home/quotation/draft');
        }
    }

    public function createFromConsument($id)
    {
        $consument = Consument::find($id);

        return view('marketing.quotation.add_consument')
        ->with('title', 'Add new Quotation')
        ->with('menu', 'quotation')
        ->with('consument', $consument);
    }

    public function storeFromConsument(Request $request, $id)
    {
        if (isset($request->save)) {
            $this->validate($request, [
                'project' => 'required',
                'time' => 'required'
            ]);

            $total = 0;
            foreach($request->field_price as $add) {
                $total = $total + $add;
            }

            $quotation = new Quotation;
            $quotation->project = $request->project;
            $quotation->price = $total;
            $quotation->time = $request->time;
            $quotation->consument = $id;
            $quotation->user = Auth::user()->id;
            $quotation->save();

            //create quotation number
            $urutan = Quotation::
            whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->where('quo', '!=', '')
            ->count();
            $urutan = $urutan + 1;
            if ($urutan < 10) {
                $urutan  = '00'. $urutan;
            } else if ($urutan < 100 && $urutan > 9) {
                $urutan = '0' .$urutan;
            }

            //update last inserted quotation
            $new = Quotation::find($quotation->id);
            $new->quo = 'QUO' . date('m') . date('y') . $urutan;
            $new->save();

            $current_quo = $new->id;

            if ($total > 0) {
                foreach ($request->field_name as $key=>$iter) {
                    $detail = new QuotationDetail();
                    $detail->id_quo = $current_quo;
                    $detail->item = $iter;
                    $detail->harga = $request->field_price[$key];
                    $detail->save();
                }
            }

            return redirect('home/quotation');
        }

        if (isset($request->draft)) {
            $this->validate($request, [
                'project' => 'required'
            ]);

            $total = 0;
            foreach($request->field_price as $add) {
                $total = $total + $add;
            }

            $quotation = new Quotation;
            $quotation->consument = $id;
            $quotation->user = Auth::user()->id;
            $quotation->project = $request->project;
            $quotation->price = $total;

            if ($request->time != '') {
                $quotation->time = $request->time;
            }

            $quotation->save();

            $current_quo = $quotation->id;
            if ($total > 0) {
                foreach ($request->field_name as $key=>$iter) {
                    $detail = new QuotationDetail();
                    $detail->id_quo = $current_quo;
                    $detail->item = $iter;
                    $detail->harga = $request->field_price[$key];
                    $detail->save();
                }
            }

            return redirect('home/quotation/draft');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $checker = Quotation::find($id);
        if ($checker->user != Auth::user()->id) {
            //prevent URL injection
            return redirect('/home/quotation');
        }

        $data = DB::table('quotations')
        ->join('consuments', 'quotations.consument', '=', 'consuments.id')
        ->select('quotations.*', 'consuments.name', 'consuments.address', 'consuments.phone', 'consuments.email')
        ->first();

        $item = DB::table('quotation_details')
            ->where('quotation_details.id_quo','=',$id)
            ->select('quotation_details.*')
            ->get();

        return view('marketing.quotation.show')
        ->with('title', $data->quo)
        ->with('menu', 'quotation')
        ->with('quotation', $data)
        ->with('items', $item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quotation = Quotation::find($id);
        // prevent URL injection
        if ($quotation->user != Auth::user()->id) {
            return redirect('/home/quotation');
        }
        if ($quotation->status === 'accepted' || $quotation->status === 'rejected') {
            return redirect('/home/quotation');
        }

        return view('marketing.quotation.edit')
        ->with('title', 'Edit Quotation')
        ->with('menu', 'quotation')
        ->with('quotation', $quotation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->save)) {
            $this->validate($request, [
                'project' => 'required',
                'time' => 'required',
            ]);

            $quotation = Quotation::find($id);
            $quotation->project = $request->project;
            $quotation->time = $request->time;

            if ($quotation->quo === '') {
                $urutan = Quotation::
                whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))
                ->where('quo', '!=', '')
                ->count();
                $urutan = $urutan + 1;
                if ($urutan < 10) {
                    $urutan  = '00'. $urutan;
                } else if ($urutan < 100 && $urutan > 9) {
                    $urutan = '0' .$urutan;
                }

                $quotation->quo = 'QUO' . date('m') . date('y') . $urutan;
            }

            $quotation->save();
            return redirect('home/quotation');
        }

        if (isset($request->draft)) {
            $this->validate($request, [
                'project' => 'required'
            ]);

            $quotation = Quotation::find($id);
            $quotation->project = $request->project;
            $quotation->price = $request->price;

            if ($request->time != '') {
                $quotation->time = $request->time;
            }

            $quotation->save();
            return redirect('/home/quotation/draft');
        }
    }

    public function editItem($id)
    {
        $items = QuotationDetail::where('id_quo', '=', $id)->get();

        return view('marketing.quotation.edit_item')
			 ->with('title', 'Edit Quotation')
			->with('menu', 'quotation')
				->with('items', $items)
            ->with('id', $id);
    }

    public function addItem(Request $request, $id)
    {
        foreach($request->field_name as $index=>$a) {
            $item = new QuotationDetail();
            $item->id_quo = $id;
            $item->item = $a;

            if ($request->field_price[$index] > 0) {
                $item->harga = $request->field_price[$index];
            } else {
                $item->harga = 0;
            }

            $item->save();
        }

        $total = 0;
        $price = QuotationDetail::where('id_quo', '=', $id)->get();
        foreach($price as $a) {
            $total = $total + $a->harga;
        }
        $quo = Quotation::find($id);
        $quo->price = $total;
        $quo->save();

        return redirect('home/quotation/' . $id . '/edit/item');
    }

    public function editItemView($id, $id_item)
    {
        $item = QuotationDetail::where('id', '=', $id_item)->first();
        return view('marketing.quotation.edit_item_detail')
            ->with('title', 'Edit Quotation')
			->with('menu', 'quotation')
			->with('item', $item)
            ->with('id', $id);
    }

    public function updateItem($id, $id_item, Request $request)
    {
        $item = QuotationDetail::where('id', '=', $id_item)->first();
        $item->item = $request->item;
        $item->harga = $request->price;
        $item->save();

        $total = 0;
        $iter = QuotationDetail::where('id_quo', '=', $id)->get();
        foreach($iter as $a) {
            $total = $total + $a->harga;
        }

        $quo = Quotation::find($id);
        $quo->price = $total;
        $quo->save();

        return redirect('home/quotation/' . $id . '/edit/item');
    }

    public function destroyItem($id_item) {
        $item = QuotationDetail::where('id', '=', $id_item)->first()->delete();

        return redirect(url()->previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
