<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Contract;
use App\Quotation;
use Auth;
use File;
use Storage;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data = DB::table('contracts')
      ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
      ->select('contracts.*')
      ->where('oncontract', '=', 'yes')
      ->where('quotations.user', Auth::user()->id)
      ->get();

      return view('marketing.contract.index')
      ->with('title', 'Contracts')
      ->with('menu', 'contract')
      ->with('contracts', $data);
  }

  public function search(Request $request){   
      if($request->search)
      {

        $data = DB::table('contracts')
        ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
		->select('contracts.*','quotations.quo')
        ->where('quotations.user', Auth::user()->id)
        ->where('contracts.quo','like','%'.$request->search.'%')
        ->get();

        if($data){
            $count=1;
            
            foreach($data as $key => $data){
            $contact= ' <a href="/contracts/'.$data->file.'" class="btn btn-primary btn-xs"><i class="fa fa-fw fa-file-pdf-o"></i></a>';

                echo'<tr>.
                <td>'.$count.'</td>.
                <td>'.$data->quo.'</td>.
                <td>'.$data->name.'</td>.
                <td>'.$data->con.'</td>.
                <td>'.$data->desc.'</td>.
                <td>'. '<a href="/home/contract/' . $data->id . '/edit" class="btn btn-default btn-xs"><i class="fa fa-fw fa-pencil"></i></a>'.' '.$contact.'</td></tr>';
                $count=$count+1;
            }
        }
    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $quotation = Quotation::
      where('status', 'accepted')
      ->where('user', Auth::user()->id)
      ->where('oncontract', 'no')
      ->get();

      return view('marketing.contract.add')
      ->with('title', 'Add new Contract')
      ->with('menu', 'contract')
      ->with('quotation', $quotation);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'quo' => 'required',
          'name' => 'required',
          'desc' => 'required',
          'file' => 'required|file|mimetypes:application/pdf'
      ]);

        $contract = new Contract;
        $contract->quo = $request->quo;
        $contract->name = $request->name;
        $contract->desc = $request->desc;

        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'Contracts-' . $request->quo . '.' . $ext;
        $request->file('file')->move('contracts/', $filename);

        $contract->file = $filename;
        $contract->save();
		
		//create quotation number
            $urutan = Contract::
            whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->where('con', '!=', '')
            ->count();
            $urutan = $urutan + 1;
            if ($urutan < 10) {
                $urutan  = '00'. $urutan;
            } else if ($urutan < 100 && $urutan > 9) {
                $urutan = '0' .$urutan;
            }

            //update last inserted quotation
            $new = Contract::find($contract->id);
            $new->con = 'CON' . date('m') . date('y') . $urutan;
            $new->save();

        // update current quotation
        $quo = Quotation::where('quo', '=', $request->quo)->first();
        $quo->oncontract = 'yes';
        $quo->save();

        return redirect('home/contract');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contract = Contract::find($id);

        return view('marketing.contract.edit')
        ->with('title', 'Edit Existing Contract')
        ->with('menu', 'contract')
        ->with('contract', $contract);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contract = Contract::find($id);

        $this->validate($request, [
          'name' => 'required',
          'desc' => 'required',
          'file' => 'file|mimetypes:application/pdf'
      ]);

        $contract->name = $request->name;
        $contract->desc = $request->desc;

        if ($request->file('file')) {
          File::delete('contracts/' . $contract->file);

          $ext = Input::file('file')->getClientOriginalExtension();
          $filename = 'Contracts-' . $contract->quo . '.' . $ext;
          $request->file('file')->move('contracts', $filename);
      }


        $contract->save();

      return redirect('home/contract');
  }

   public function show($id)
    {
       $checker = DB::table('contracts')
		  ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
		  ->select('contracts.*','quotations.user')
		  ->where('oncontract', '=', 'yes')
		  ->where('quotations.user', Auth::user()->id)
		  ->where('contracts.id','=',$id)->get()
		  ->first();
        if ($checker->user != Auth::user()->id) {
            //prevent URL injection
            return redirect('/home/contract');
        }

       $data = DB::table('contracts')
      ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
      ->select('contracts.*','quotations.project')
      ->where('oncontract', '=', 'yes')
      ->where('quotations.user', Auth::user()->id)
	  ->where('contracts.id', '=', $id)
      ->first();

      return view('marketing.contract.show')
      ->with('title', 'Contracts')
      ->with('menu', 'contract')
      ->with('contract', $data);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
