<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use App\User;
use App\Consument;
use DB;
use Excel;

class AdminQuotationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('superadmin');
    }

    public function index(Request $request)
    {
        if ($request->has('user')) {
            $quotations = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.user', '=', $request->user)
                ->where('quotations.quo', '!=', '')
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        } else {
            $quotations = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.quo', '!=', '')
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        }

        $marketing = User::where('role', '=', 'marketing')
            ->get();

        return view('superadmin.quotation.index')
            ->with('quotations', $quotations)
            ->with('marketing', $marketing);
    }

    public function draft(Request $request)
    {
        if ($request->has('user')) {
            $quotations = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.user', '=', $request->user)
                ->where('quotations.quo', '=', '')
                ->paginate(10);
        } else {
            $quotations = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.quo', '=', '')
                ->paginate(10);
        }

        $marketing = User::where('role', '=', 'marketing')
            ->get();

        return view('superadmin.quotation.draft')
            ->with('quotations', $quotations)
            ->with('marketing', $marketing);
    }

    public function view($id)
    {
        $quotation = DB::table('quotations')
            // ->join('users', 'quotations.user', '=', 'users.id')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name as c_name', 'consuments.email as c_email', 'consuments.phone as c_phone', 'consuments.address as c_address')
            ->where('quotations.id', '=', $id)
            ->first();

        $items = DB::table('quotation_details')
            ->select('quotation_details.*')
            ->where('quotation_details.id_quo', '=', $quotation->id)
            ->get();

        return view('superadmin.quotation.view')
            ->with('quotation', $quotation)
            ->with('items', $items);
    }

    public function export()
    {
        $marketing = DB::table('users')
            ->where('role', '=', 'marketing')
            ->get();

        return view('superadmin.quotation.export')
            ->with('marketing', $marketing);
    }

    public function toExcel(Request $request)
    {
        if ($request->marketing !== 'all') {
            $takeName = DB::table('users')
                ->select('users.name')
                ->where('users.id', '=', $request->marketing)
                ->first();
            $name = $takeName->name;
        } else {
            $name = 'All';
        }

        if ($request->marketing === 'all') {
            $GLOBALS['data'] = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                // ->where('quotations.user', '=', $request->user)
                ->whereYear('quotations.created_at', '=', $request->quotation_year)
                ->whereMonth('quotations.created_at', '=', $request->quotation_month)
                ->get();
        } else {
            $GLOBALS['data'] = DB::table('quotations')
                ->join('consuments', 'quotations.consument', '=', 'consuments.id')
                ->join('users', 'quotations.user', '=', 'users.id')
                ->select('quotations.*', 'consuments.name as c_name', 'users.name as user_name')
                ->where('quotations.user', '=', $request->marketing)
                ->whereYear('quotations.created_at', '=', $request->quotation_year)
                ->whereMonth('quotations.created_at', '=', $request->quotation_month)
                ->get();
        }

        Excel::create('Data_Quotations_' . $name . '_' . $request->quotation_year . '-' . $request->quotation_month, function ($excel) {
            $excel->sheet('Quotations', function ($sheet) {
                $sheet->loadView('superadmin.excel.quotation')
                    ->with('data', $GLOBALS['data']);
            });
        })->download('xlsx');
    }

    public function exportSingle($id)
    {
        $check = Quotation::find($id);
        if ($check->quo === '') {
            return;
        }
        $GLOBALS['data'] = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name as c_name', 'consuments.email as c_email', 'consuments.phone as c_phone', 'consuments.address as c_address')
            ->where('quotations.id', '=', $id)
            ->first();

        $GLOBALS['item'] = DB::table('quotation_details')
            ->select('quotation_details.*')
            ->where('quotation_details.id_quo', '=', $id)
            ->get();

        Excel::create('' . $GLOBALS['data']->quo, function ($excel) {
            $excel->sheet('Quotation', function($sheet) {
                $sheet->loadView('superadmin.excel.single_quotation')
                    ->with('data', $GLOBALS['data'])
                    ->with('item', $GLOBALS['item']);
            });
        })->download('xlsx');

        return redirect(url('superadmin/quotation/index'));
    }
}
