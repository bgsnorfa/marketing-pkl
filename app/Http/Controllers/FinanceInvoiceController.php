<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use App\Invoices;
use App\Contract;
use App\Quotation;
use Redirect;
use DB;
use Auth;
use Carbon\Carbon;

class FinanceInvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('finance');
    }

    public function index(Request $request)
    {

        $data =  DB::table('invoices')
        ->join('quotations','quotations.quo','=','invoices.quo')
        ->select('invoices.*')
        ->get();

        return view('finance.invoice.index')
        ->with('title', 'Invoice')
        ->with('menu', 'invoice')
        ->with('quotation', $data);

    }
    public function indexPay(Request $request)
    {

		$data =  Quotation::where('quotations.quo', '!=', '')
        ->where('quotations.oncontract', '=', 'yes')
		->where('quotations.status', '=', 'accepted')->get();

		return view('finance.invoice.pay')
        ->with('title', 'Invoice')
        ->with('menu', 'invoice')
        ->with('quotation', $data);

    }
public function search(Request $request){
      if($request->search)
      {

        $data =  DB::table('invoices')
        ->join('quotations','quotations.quo','=','invoices.quo')
        ->select('invoices.*','quotations.user')
        ->where('invoices.quo','like','%'.$request->search.'%')
        ->get();

        if($data){
         $count=1;
         foreach($data as $key => $data){
            if($data->status != 'paid')
            {
              $contact='<a href="/finance/invoice/' .$data->id. '/payinvoice" class="btn btn-xs btn-success">Pay</a>';
            }
            else
            {
                $contact='<a href="/invoices/' . $data->file.' "class="btn btn-xs btn-primary">Show</a>';
            }
            if($data->duedate < Carbon::now() && $data->status != 'paid'){
            echo'<tr bgcolor="#FF0000">';
        }
            else{
            echo'<tr>';
    }
            echo'<td>'.$count.'</td>.
            <td>'.$data->inv.'</td>.
            <td>'.$data->quo.'</td>.
            <td>'.$data->item.'</td>.
            <td>'.$data->amount.'</td>.
            <td>'.$data->duedate.'</td>.
            <td>'.$data->status.'</td>.
            <td>'.$contact.'</td></tr>';
            $count=$count+1;
        }
    }
}
}

    public function search1(Request $request){
      if($request->search)
      {
      $data =  Quotation::where('quotations.quo', '!=', '')
        ->where('quotations.status', '=', 'accepted')
        ->where('quotations.oncontract', '=', 'yes')
        ->where('quotations.quo','like','%'.$request->search.'%')
        ->get();


        if($data){
         $count=1;
         foreach($data as $key => $data){
            if($data->price != $data->bill)
            {
                $contact='<a href="/finance/invoice/' .$data->id. '/payment" class="btn btn-xs btn-success">Add Payment</a>';
            }
            else
            {
                $contact='';
            }

            echo'<tr>.
            <td>'.$count.'</td>.
            <td>'.$data->quo.'</td>.
            <td>'. number_format($data->price).'</td>.
            <td>'. number_format($data->bill).'</td>.
            <td>'. number_format($data->paid).'</td>.
            <td>'. number_format($data->price - $data->paid).'</td>.
            <td>'. '<a href="/finance/invoice/' . $data->id . '/view" class="btn btn-xs btn-primary">View</a>'.' '.$contact.'</td></tr>';
            $count=$count+1;
        }
    }
}
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$id)
    {
		$data =  Quotation::find($id);
        if ($data->oncontract === 'no') {
			return redirect('finance/invoice/view');
		}
		if ($data->price == $data->paid or $data->status != 'accepted')
			{
			  return redirect('finance/invoice');
			}
		$quo = $data->quo;
		$invoices= Invoices::where('quo','=',$quo)->get();

		$total=0;
		foreach($invoices as $inv){
			$total=$total+$inv->amount;
		}


		return view('finance.invoice.add')
		->with('title', 'Invoice')
		->with('menu', 'invoice')
		->with('total', $total)
		->with('quotation', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $result = new Invoices;

		  $this->validate($request, [
          'amount' => 'required',
          'item' => 'required',
          'duedate' => 'required',

      ]);

        $result->quo = $request->quo;
		$result->item = $request->item;
        $result->amount = $request->amount;
        $result->duedate = $request->duedate;
        $result->save();

		$addPaid =  Quotation::where('quo','=', $request->quo)->first();
		$curent=$addPaid->bill;
		$addPaid->bill=$curent+$request->amount;
		$addPaid->save();

		$urutan = Invoices::
            whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->where('inv', '!=', '')
            ->count();
            $urutan = $urutan + 1;
            if ($urutan < 10) {
                $urutan  = '00'. $urutan;
            } else if ($urutan < 100 && $urutan > 9) {
                $urutan = '0' .$urutan;
            }

            //update last inserted quotation
            $new = Invoices::find($result->id);
            $new->inv= 'INV' . date('m') . date('y') . $urutan;
            $new->save();

        return redirect('finance/invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Quotation::find($id);
		$inv= Invoices::where('quo','=',$data->quo)->get();

        return view('finance.invoice.show')
            ->with('title', $data->quo)
            ->with('menu', 'quotation')
            ->with('quotation', $data)
            ->with('invoice', $inv);
    }

     public function payInvoice($id)
    {
        $invoice = Invoices::find($id);
        return view('finance.invoice.payInvoice')->with('invoice',$invoice);
    }

    public function storePay(Request $request, $id)
    {
        $invoice = Invoices::find($id);

        $this->validate($request, [
            'file' => 'required|file|mimetypes:application/pdf',
        ]);

        $invoice->status = 'paid';

        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'Invoices-' . $invoice->inv . '.' . $ext;
        $request->file('file')->move('invoices/', $filename);

        $invoice->file = $filename;

        $addPaid =  Quotation::where('quo','=', $invoice->quo)->first();
        $curent=$addPaid->paid;
        $addPaid->paid=$curent+$invoice->amount;
        $addPaid->save();
        $invoice->save();
        return redirect('finance/invoice');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
