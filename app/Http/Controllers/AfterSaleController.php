<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Quotation;
use App\AfterSale;
use Auth;
use File;
use Storage;
use Carbon\Carbon;

class AfterSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
      $this->middleware('auth');
      $this->middleware('marketing');
    }

    public function index()
    {
      $data = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*','quotations.project')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)
        ->get();

      return view('marketing.aftersale.index')
        ->with('title', 'After Sales')
        ->with('menu', 'aftersale')
        ->with('aftersales', $data);
    }

    public function search(Request $request){
      if($request->search)
      {

          $data = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)
        ->where('quotations.quo','like','%'.$request->search.'%')
        ->get();

        if($data){
         $count=1;
         foreach($data as $key => $data){

                $contact='<a href="/home/aftersale/' .$data->id. '/edit" class="btn btn-xs btn-default">Edit</a>';

            echo'<tr>.
            <td>'.$count.'</td>.
            <td>'.$data->quo.'</td>.
            <td>'.$data->startdate.'</td>.
            <td>'.$data->enddate.'</td>.
            <td>'. '<a href="/home/aftersale/' . $data->id . '/view" class="btn btn-xs btn-primary">Show</a>'.' '.$contact.'</td></tr>';
            $count=$count+1;
        }
    }
}
}
 public function search1(Request $request){
      if($request->search)
      {

         $expDate = Carbon::now()->addDays(30);
		$data = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)
		->whereDate('enddate', '<',$expDate)
        ->where('quotations.quo','like','%'.$request->search.'%')
        ->get();

        if($data){
         $count=1;
         foreach($data as $key => $data){

                $contact='<a href="/home/aftersale/' .$data->id. '/edit" class="btn btn-xs btn-default">Edit</a>';

            echo'<tr>.
            <td>'.$count.'</td>.
            <td>'.$data->quo.'</td>.
            <td>'.$data->startdate.'</td>.
            <td>'.$data->enddate.'</td>.
            <td>'. '<a href="/home/aftersale/' . $data->id . '/view" class="btn btn-xs btn-primary">Show</a>'.' '.$contact.'</td></tr>';
            $count=$count+1;
        }
    }
}
}
	public function review()
    {
		$expDate = Carbon::now()->addDays(30);
		$aftersales = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)
		->whereDate('enddate', '<',$expDate)
        ->paginate(10);

      return view('marketing.aftersale.expired')
        ->with('title', 'After Sales')
        ->with('menu', 'aftersale')
        ->with('aftersales', $aftersales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $quotation = Quotation::
        where('status', 'accepted')
        ->where('user', Auth::user()->id)
        ->where('onaftersale', 'no')
        ->get();

      return view('marketing.aftersale.add')
        ->with('title', 'Add new After Sale')
        ->with('menu', 'aftersale')
        ->with('quotation', $quotation);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'quo' => 'required',
        'startdate' => 'required',
        'enddate' => 'required',
        'file' => 'required|file|mimetypes:application/pdf'
      ]);

      $aftersale = new AfterSale;
      $aftersale->quo = $request->quo;
      $aftersale->startdate = $request->startdate;
      $aftersale->enddate = $request->enddate;

      $ext = Input::file('file')->getClientOriginalExtension();
      $filename = 'AfterSales-' . $request->quo . '.' . $ext;
      $request->file('file')->move('aftersales/', $filename);

      $aftersale->file = $filename;
      $aftersale->save();

      // update current quotation
      $quo = Quotation::where('quo', '=', $request->quo)->first();
      $quo->onaftersale = 'yes';
      $quo->save();

      return redirect('home/aftersale');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $aftersale = AfterSale::find($id);

      return view('marketing.aftersale.edit')
        ->with('title', 'Edit Existing After Sale')
        ->with('menu', 'aftersale')
        ->with('aftersale', $aftersale);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $aftersale = AfterSale::find($id);

      $this->validate($request, [
        'startdate' => 'required',
        'enddate' => 'required|after:startdate',
        'file' => 'file|mimetypes:application/pdf'
      ]);

		// $aftersale->startdate = $request->startdate;
		$aftersale->enddate = $request->enddate;

      if ($request->file('file')) {
        File::delete('aftersales/' . $aftersale->file);

        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'AfterSales-' . $aftersale->quo . '.' . $ext;
        $request->file('file')->move('aftersales', $filename);
      }
      $aftersale->save();


      return redirect('home/aftersale');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
