<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Quotation;
use App\Consument;      
use App\Contract;
use App\Invoices;
use App\AfteSale;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

class MarketingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('marketing');
    }

    public function index()
    {
		$cons = Consument::where('user','=',Auth::user()->id)->count();
		
		$x = Consument::where('user','=',Auth::user()->id)->get();
        $cons1=0;
        foreach($x as $key => $x){
        $a = Quotation::where('consument','=',$x->id)->count();

            if($a != 0)
            {               
                $cons1=$cons1+1;
            }
        }

        $y = Consument::where('user','=',Auth::user()->id)->get();
        $cons2=0;
        foreach($y as $key => $y){
        $b = Quotation::where('consument','=',$y->id)->where('oncontract','=','yes')->count();

            if($b != 0)
            {               
                $cons2=$cons2+1;
            }
        }
		$quo = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.user', '=', Auth::user()->id)
            ->get();
			
		$quoall = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.user', '=', Auth::user()->id)
            ->count();
			
		$quowait = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'waiting')
            ->where('quotations.user', '=', Auth::user()->id)
            ->count();
			
		$quoacc = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'accepted')
            ->where('quotations.user', '=', Auth::user()->id)
            ->count();
			
		$quorejec = DB::table('quotations')
            ->join('consuments', 'quotations.consument', '=', 'consuments.id')
            ->select('quotations.*', 'consuments.name')
            ->where('quotations.quo', '!=', '')
            ->where('quotations.status', '=', 'rejected')
            ->where('quotations.user', '=', Auth::user()->id)
            ->count();
			
		$cont=Contract::count();
	//	$cont1=DB::table('contracts')
	//	->join('quotations','quotation.qui','=','contracts.quo')
	//	->where('quotations.user', '=', Auth::user()->id)
	//	-count();
		
		$inv=Quotation::where('quotations.quo', '!=', '')
		->where('quotations.user', '=', Auth::user()->id)
		->where('quotations.status', '=', 'accepted')->get();
        
            
      $as = DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)
        ->count();

        $expDate = Carbon::now()->addDays(30); 
        $variable2 =  DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)->whereDate('enddate', '<',$expDate)->get()->count();

        $aftersale2 = $as-$variable2;

        return view('marketing.index')
        ->with('title', 'Dashboard')
        ->with('consument', $cons)
        ->with('contract', $cont)
        ->with('consument1', $cons1)
        ->with('consument2', $cons2)
        ->with('quotation', $quo)
        ->with('quotation1', $quoall)
        ->with('quotation2', $quowait)
        ->with('quotation3', $quoacc)
        ->with('quotation4', $quorejec)
        ->with('invoice', $inv)
        ->with('aftersale', $as)
        ->with('aftersale1', $variable2)
        ->with('aftersale2', $aftersale2)
        ->with('menu', 'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
