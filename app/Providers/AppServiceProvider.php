<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\AfterSale;
use App\Quotation;
use App\Consument;
use App\Contract;
use App\Invoice;
use App\AfteSale;
use Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use DB;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	view()->composer('layouts.marketing', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now()->addDays(30);
		 $variable2 =  DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)->whereDate('enddate', '<',$expDate)->get()->count();
        $view->with('variable2', $variable2);
    });
	view()->composer('layouts.marketing1', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now()->addDays(30);
		 $variable2 =  DB::table('after_sales')
        ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
        ->select('after_sales.*')
        ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)->whereDate('enddate', '<',$expDate)->get()->count();
        $view->with('variable2', $variable2);
    });
	
	view()->composer('layouts.marketing', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now();
		$variable1 =  DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->where('quotations.user', Auth::user()->id)
        ->where('invoices.status','!=', 'paid')
		->whereDate('duedate', '<',$expDate)->get()->count();
        $view->with('variable1', $variable1);
    });
	
	view()->composer('layouts.marketing1', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now();
		$variable1 =  DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->where('quotations.user', Auth::user()->id)
        ->where('invoices.status','!=', 'paid')
		->whereDate('duedate', '<',$expDate)->get()->count();
        $view->with('variable1', $variable1);
    });
    view()->composer('layouts.finance1', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now()->addDays(30);
		 $variable2 =  DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->select('invoices.*')
        // ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)->whereDate('duedate', '<',$expDate)->get()->count();
        $view->with('variable2', $variable2);
    });
    view()->composer('layouts.finance', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now()->addDays(30);
		 $variable2 =  DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->select('invoices.*')
        // ->where('onaftersale', '=', 'yes')
        ->where('quotations.user', Auth::user()->id)->whereDate('duedate', '<',$expDate)->get()->count();
        $view->with('variable2', $variable2);
    });
	view()->composer('layouts.finance', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now();
		$variable1 =  DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->where('invoices.status','!=', 'paid')
		->whereDate('duedate', '<',$expDate)->get()->count();
        $view->with('variable1', $variable1);
    });
	view()->composer('layouts.finance1', function($view)
    {
//		$variable2 = AfterSale::get()->count();
		$expDate = Carbon::now();
		$variable1 =  DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->where('invoices.status','!=', 'paid')
		->whereDate('duedate', '<',$expDate)->get()->count();
        $view->with('variable1', $variable1);
    });

        Schema::defaultStringLength('191');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
