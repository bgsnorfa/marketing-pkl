<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect('login');
});

Route::get('/superadmin', 'HomeController@index');
// route for marketing
Route::get('/home', 'MarketingController@index');

// route for consument
Route::get('/home/consument', 'ConsumentController@index');
Route::post('/home/consument/search', 'ConsumentController@search');
// contact consument
Route::get('/home/consument/{id}/contact', 'ConsumentController@contact');
// create new consument
Route::get('/home/consument/add', 'ConsumentController@create');
Route::post('/home/consument/add', 'ConsumentController@store');
// edit consument data
Route::get('/home/consument/{id}/edit', 'ConsumentController@edit');
Route::post('/home/consument/{id}/edit', 'ConsumentController@update');

// route for quotation
Route::get('/home/quotation', 'QuotationController@index');
Route::post('/home/quotation/search', 'QuotationController@search');
Route::post('/home/quotation/search1', 'QuotationController@search1');
Route::get('/home/quotation/draft', 'QuotationController@draft');
// accept and reject quotation
Route::get('/home/quotation/{id}/accept', 'QuotationController@accept');
Route::post('/home/quotation/{id}/accept', 'QuotationController@storeaccept');
Route::get('/home/quotation/{id}/reject', 'QuotationController@reject');
// create quotation
Route::get('/home/quotation/add', 'QuotationController@create');
Route::post('/home/quotation/add', 'QuotationController@store');
// create quotation from consument
Route::get('/home/quotation/add/consument/{id}', 'QuotationController@createFromConsument');
Route::post('/home/quotation/add/consument/{id}', 'QuotationController@storeFromConsument');
// edit quotation
Route::get('/home/quotation/{id}/edit', 'QuotationController@edit');
Route::post('/home/quotation/{id}/edit', 'QuotationController@update');
// edit quotation item
Route::get('/home/quotation/{id}/edit/item', 'QuotationController@editItem');
Route::post('/home/quotation/{id}/edit/item/add', 'QuotationController@addItem');
Route::get('/home/quotation/{id}/edit/item/{id_item}', 'QuotationController@editItemView');
Route::post('/home/quotation/{id}/edit/item/{id_item}', 'QuotationController@updateItem');
Route::get('/home/quotation/{id}/edit/item/{id_item}/delete', 'QuotationController@destroyItem');
// show specific quotation
Route::get('/home/quotation/{id}', 'QuotationController@show');

// route for contract
Route::get('/home/contract', 'ContractController@index');
Route::get('/home/contract/add', 'ContractController@create');
Route::post('/home/contract/add', 'ContractController@store');
Route::get('/home/contract/{id}/edit', 'ContractController@edit');
Route::post('/home/contract/{id}/edit', 'ContractController@update');
Route::post('/home/contract/search', 'ContractController@search');
Route::get('/home/contract/{id}', 'ContractController@show');


//retrieve Invoice
Route::get('/home/invoice', 'InvoicesController@index');
Route::get('/home/invoice/view', 'InvoicesController@indexPay');
//display invoices add form
Route::get('/home/invoice/{id}/payment', 'InvoicesController@create');
//store invoices data
Route::post('/home/invoice/add', 'InvoicesController@store');
Route::get('/home/invoice/{id}/edit', 'InvoicesController@edit');
Route::post('/home/invoice/{id}/edit', 'InvoicesController@update');
//display invoices based on quotation number
Route::get('/home/invoice/{id}/view', 'InvoicesController@show');
Route::get('/home/invoice/{id}/payinvoice', 'InvoicesController@payInvoice');
Route::get('/home/invoice/{id}/print', 'InvoicesController@recap');
Route::post('/home/invoice/{id}/payinvoice', 'InvoicesController@storePay');
// Route::post('/admin/invoice', 'InvoicesController@index');
Route::post('/home/invoice/search', 'InvoicesController@search');
Route::post('/home/invoice/search1', 'InvoicesController@search1');

// route for aftersale
Route::get('/home/aftersale', 'AfterSaleController@index');
Route::get('/home/aftersalereview', 'AfterSaleController@review');
Route::get('/home/aftersale/add', 'AfterSaleController@create');
Route::post('/home/aftersale/add', 'AfterSaleController@store');
Route::get('/home/aftersale/{id}/edit', 'AfterSaleController@edit');
Route::post('/home/aftersale/{id}/edit', 'AfterSaleController@update');
Route::post('/home/aftersale/search', 'AfterSaleController@search');
Route::post('/home/aftersale/search1', 'AfterSaleController@search1');

// route for aftersale

//route for superadmin
// Route::get('/superadmin', 'SuperAdminController@index');
Route::get('/superadmin', 'SuperAdminController@index');

// superadmin consument
Route::get('/superadmin/consument', 'AdminConsumentController@index');
Route::get('/superadmin/consument/export', 'AdminConsumentController@export');
use App\Consument;
Route::get('/superadmin/consument/ajax', function () {
    $consument_year = $_GET['year'];
    $get = Consument::whereyear('created_at', '=', $consument_year)
        ->get();
    $month = array();
    foreach ($get as $a) {
        if (! in_array($a->created_at->format('m'), $month)) {
            array_push($month, $a->created_at->format('m'));
        }
    }

    return $month;
});
Route::post('/superadmin/consument/export', 'AdminConsumentController@toExcel');
Route::get('/superadmin/consument/{id}', 'AdminConsumentController@view');


// superadmin quotation
Route::get('/superadmin/quotation', 'AdminQuotationController@index');
Route::get('/superadmin/quotation/draft', 'AdminQuotationController@draft');
Route::get('/superadmin/quotation/export', 'AdminQuotationController@export');
Route::get('/superadmin/quotation/export/{id}', 'AdminQuotationController@exportSingle');
use App\Quotation;
Route::get('/superadmin/quotation/ajaxyear', function () {
    $marketing = $_GET['marketing'];
    if ($marketing === 'all') {
        $get = Quotation::get();
    } else {
        $get = Quotation::where('user', '=', $marketing)
            ->get();
    }

    $year = array();
    foreach ($get as $a) {
        if (! in_array($a->created_at->format('Y'), $year)) {
            array_push($year, $a->created_at->format('Y'));
        }
    }

    return $year;
});
Route::get('/superadmin/quotation/ajaxmonth', function () {
    $marketing = $_GET['marketing'];
    $quotation_year = $_GET['year'];

    if ($marketing === 'all') {
        $get = Quotation::whereYear('created_at', '=', $quotation_year)
            ->get();
    } else {
        $get = Quotation::where('user', '=', $marketing)
            ->whereYear('created_at', '=', $quotation_year)
            ->get();
    }

    $month = array();
    foreach ($get as $a) {
        if (! in_array($a->created_at->format('m'), $month)) {
            array_push($month, $a->created_at->format('m'));
        }
    }

    return $month;
});
Route::post('/superadmin/quotation/export', 'AdminQuotationController@toExcel');
Route::get('/superadmin/quotation/{id}', 'AdminQuotationController@view');
Route::get('/superadmin/quotation/draft/{id}', 'AdminQuotationController@view');

// superadmin contract
Route::get('/superadmin/contract', 'AdminContractController@index');
use App\Contract;
Route::get('/superadmin/contract/ajaxyear', function () {
    $marketing = $_GET['marketing'];
    if ($marketing === 'all') {
        $get = DB::table('contracts')
            ->select('contracts.*')
            ->get();
    } else {
        $get = DB::table('contracts')
            ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
            ->join('users', 'quotations.user', '=', 'users.id')
            ->select('contracts.*')
            ->where('quotations.user', '=', $marketing)
            ->get();
    }

    $year = array();
    foreach ($get as $a) {
        if (! in_array(date('Y', strtotime($a->created_at)), $year)) {
            array_push($year, date('Y', strtotime($a->created_at)));
        }
    }

    return $year;
});
Route::get('/superadmin/contract/ajaxmonth', function () {
    $marketing = $_GET['marketing'];
    $contract_year = $_GET['year'];

    if ($marketing === 'all') {
        $get = DB::table('contracts')
            ->whereYear('contracts.created_at', '=', $contract_year)
            ->get();
    } else {
        $get = DB::table('contracts')
            ->join('quotations', 'contracts.quo', '=', 'quotations.quo')
            ->join('users', 'quotations.user', '=', 'users.id')
            ->select('contracts.*')
            ->where('quotations.user', '=', $marketing)
            ->whereYear('contracts.created_at', '=', $contract_year)
            ->get();
    }

    $month = array();
    foreach ($get as $a) {
        if (! in_array(date('m', strtotime($a->created_at)), $month)) {
            array_push($month, date('m', strtotime($a->created_at)));
        }
    }

    return $month;
});
Route::get('/superadmin/contract/export', 'AdminContractController@export');
Route::post('/superadmin/contract/export', 'AdminContractController@toExcel');
Route::get('/superadmin/contract/{id}', 'AdminContractController@view');

// superadmin invoice
Route::get('/superadmin/invoice', 'AdminInvoiceController@index');
Route::get('/superadmin/invoice/ajaxmonth', function () {
    $inv_year = $_GET['year'];

    $get = DB::table('invoices')
        ->join('quotations', 'invoices.quo', '=', 'quotations.quo')
        ->select('invoices.*', 'quotations.quo as q_quo')
        ->whereYear('invoices.created_at', '=', $inv_year)
        ->get();

    $month = array();
    foreach ($get as $a) {
        if (! in_array(date('m', strtotime($a->created_at)), $month)) {
            array_push($month, date('m', strtotime($a->created_at)));
        }
    }

    return $month;
});
Route::get('/superadmin/invoice/export', 'AdminInvoiceController@export');
Route::post('/superadmin/invoice/export', 'AdminInvoiceController@toExcel');
Route::get('/superadmin/invoice/export/{id}', 'AdminInvoiceController@exportSingle');
Route::get('/superadmin/invoice/{id}', 'AdminInvoiceController@view');

// superadmin aftersale
Route::get('/superadmin/aftersale', 'AdminAfterSaleController@index');
use App\AfterSale;
Route::get('/superadmin/aftersale/ajaxyear', function () {
    $marketing = $_GET['marketing'];
    if ($marketing === 'all') {
        $get = DB::table('after_sales')
            ->select('after_sales.*')
            ->get();
    } else {
        $get = DB::table('after_sales')
            ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
            ->join('users', 'quotations.user', '=', 'users.id')
            ->select('after_sales.*')
            ->where('quotations.user', '=', $marketing)
            ->get();
    }

    $year = array();
    foreach ($get as $a) {
        if (! in_array(date('Y', strtotime($a->created_at)), $year)) {
            array_push($year, date('Y', strtotime($a->created_at)));
        }
    }

    return $year;
});
Route::get('/superadmin/aftersale/ajaxmonth', function () {
    $marketing = $_GET['marketing'];
    $contract_year = $_GET['year'];

    if ($marketing === 'all') {
        $get = DB::table('after_sales')
            ->whereYear('after_sales.created_at', '=', $contract_year)
            ->get();
    } else {
        $get = DB::table('after_sales')
            ->join('quotations', 'after_sales.quo', '=', 'quotations.quo')
            ->join('users', 'quotations.user', '=', 'users.id')
            ->select('after_sales.*')
            ->where('quotations.user', '=', $marketing)
            ->whereYear('after_sales.created_at', '=', $contract_year)
            ->get();
    }

    $month = array();
    foreach ($get as $a) {
        if (! in_array(date('m', strtotime($a->created_at)), $month)) {
            array_push($month, date('m', strtotime($a->created_at)));
        }
    }

    return $month;
});
Route::get('/superadmin/aftersale/export', 'AdminAfterSaleController@export');
Route::post('/superadmin/aftersale/export', 'AdminAfterSaleController@toExcel');
Route::get('/superadmin/aftersale/{id}', 'AdminAfterSaleController@view');

// superadmin user
Route::get('/superadmin/user', 'SuperAdminController@user');
Route::post('/superadmin/user', 'SuperAdminController@userStore');
Route::get('/superadmin/user/view', 'SuperAdminController@userView');
Route::get('/superadmin/user/view/{id}/edit', 'SuperAdminController@userViewEdit');
Route::post('/superadmin/user/view/{id}/edit', 'SuperAdminController@userViewUpdate');
Route::get('/superadmin/user/view/{id}/delete', 'SuperAdminController@userViewDelete');


// route for Finance
Route::get('/finance', 'FinanceController@index');

//retrieve Invoice
Route::get('/finance/invoice', 'FinanceInvoiceController@index');
Route::get('/finance/invoice/view', 'FinanceInvoiceController@indexPay');
//display invoices add form
Route::get('/finance/invoice/{id}/payment', 'FinanceInvoiceController@create');
//store invoices data
Route::post('/finance/invoice/add', 'FinanceInvoiceController@store');
//display invoices based on quotation number
Route::get('/finance/invoice/{id}/view', 'FinanceInvoiceController@show');
Route::get('/finance/invoice/{id}/payinvoice', 'FinanceInvoiceController@payInvoice');
Route::post('/finance/invoice/{id}/payinvoice', 'FinanceInvoiceController@storePay');
// Route::post('/admin/invoice', 'InvoicesController@index');
Route::post('/finance/invoice/search', 'FinanceInvoiceController@search');
Route::post('/finance/invoice/search1', 'FinanceInvoiceController@search1');
